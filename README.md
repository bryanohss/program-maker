Androidapp - Program Maker

Användaren ska kunna skapa sina egna träningsprogram. Om användaren saknar kunskapen för att skapa ett 
eget träningsprogram kommer färdiga program att finnas tillgängliga för användaren att välja mellan, baserat 
på deras träningsmål. Appen kommer också att erbjuda en katalog med näringsrika måltider för användare som vill höja sin 
kost till en högre nivå. Recept och näringsinnehåll kommer att inkluderas.

Användaren registrerar sig eller loggsar in för att komma igång.

Beskrivning av Program Maker:
Programstrukturen kommer att bestå av en eller flera "tabeller" som representerar en vecka. Användaren kan själv bestämma hur 
många träningspass varje "tabell" ska innehålla och lägga till övningar för varje pass. Färdiga kolumner för antal "set", antal
 "reps" och vikten för varje övning kommer att finnas tillgängliga. Användaren har möjlighet att redigera och lägga till fler kolumner 
 för att anpassa schemat efter behov

Min betygsambition är att satsa på en 5:a.

Funktioner:
- Registrering & Inloggning (Med eller utan Google-konto)
- Alla program sparas i databasen.
- Integrering med Google API.
- Innehållsförteckning skall finnas för varje maträtt. 
- Skapa träningsprogram.
- Dela träningsprogram med andra användare.
- Möjlighet att spara sina träningsprogram i en log.


Uppnådda krav:

    Tekniska Krav:
        Bra avvägning mellan användning av Activity och Fragments: 1p 
        Hantering av användar-input: 1p 
        Snygg användning av callbacks: 1p 
        Portabla Fragment: 1p 
        Egenskriven Adapter: 1p 
        Sökningsfunktion: 2p
        koll på inloggningsstatus: 1p

    Entreprenöriella krav:
        Firestore: 2p
        Multispråk-stöd: 1p
        Kontohantering: 1p
        Enkel inloggning: 1p 
        Tredjepartsinloggning: 1p 

    Egna Förslag:
        Exportera/Importera träningsprogram från andra användare: 1p
        innovativa och snygga designlösningar: 0.5p
    
    6/6 Milstolpar: 3p
    Totalt: 18.5p



    
