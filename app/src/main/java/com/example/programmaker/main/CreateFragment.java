package com.example.programmaker.main;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.example.programmaker.R;

public class CreateFragment extends Fragment {

    public CreateFragment() {
        // Required empty public constructor
    }

    public static CreateFragment newInstance() {

        return new CreateFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_create, container, false);
        //Get the button and set the click listener.
        Button createBtn = v.findViewById(R.id.createBtn);
        createBtn.setOnClickListener(view -> createActivity());
        return v;
    }

    private void createActivity() {
        ((NavMain)requireActivity()).createActivity();
    }
}