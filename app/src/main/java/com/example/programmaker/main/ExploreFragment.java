package com.example.programmaker.main;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.programmaker.R;
import com.example.programmaker.adapters.PShowCaseAdapter;
import com.example.programmaker.customC.WorkoutPlan;

import java.io.Serializable;
import java.util.List;
//Shows programs for the user.
public class ExploreFragment extends Fragment implements PShowCaseAdapter.ProgramAdapterListener {

    private List<WorkoutPlan> forMe;
    private List<WorkoutPlan> allPrograms;

    public ExploreFragment() {
        // Required empty public constructor
    }

    //Init with to lists.
    public static ExploreFragment newInstance(List<WorkoutPlan> forMe, List<WorkoutPlan> allPrograms) {
        ExploreFragment fragment = new ExploreFragment();
        Bundle args = new Bundle();
        args.putSerializable("forMe", (Serializable) forMe);
        args.putSerializable("allPrograms", (Serializable) allPrograms);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //Init the variables
            forMe = (List<WorkoutPlan>) getArguments().getSerializable("forMe");
            allPrograms = (List<WorkoutPlan>) getArguments().getSerializable("allPrograms");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_explore, container, false);
        //Set the up the lists.
        setUpForMeList(v);
        setUpAllLst(v);
        return v;
    }

    private void setUpAllLst(View v) {
        //Get the recyclerView
        RecyclerView lst = v.findViewById(R.id.all_programs_lst);
        //Set the layoutManager
        lst.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false));
        //Create the custom Adapter
        PShowCaseAdapter adapter = new PShowCaseAdapter(requireContext(), allPrograms, this);
        lst.setAdapter(adapter);
    }

    private void setUpForMeList(View v) {
        //Get the recyclerView
        RecyclerView lst = v.findViewById(R.id.for_me_lst);
        //Set the layoutManager
        lst.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false));
        //Create the custom Adapter
        PShowCaseAdapter adapter = new PShowCaseAdapter(requireContext(), forMe, this);
        lst.setAdapter(adapter);
    }


    @Override
    public void showProgram(WorkoutPlan chosenOne) {
        //OnClick, Call method on NavMain
        ((NavMain)requireActivity()).showCaseProgram(chosenOne);
    }
}