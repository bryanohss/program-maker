package com.example.programmaker.main;

import android.graphics.Typeface;
import android.os.Bundle;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;

import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.programmaker.R;
import com.example.programmaker.customC.RecipeHolder;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

//This fragment shows the given Recipe
public class ShowMealFragment extends Fragment {

    //Chosen One
    private HashMap<String, Object> chosenOne;

    public ShowMealFragment() {
        // Required empty public constructor
    }


    public static ShowMealFragment newInstance(RecipeHolder recipe) {
        ShowMealFragment fragment = new ShowMealFragment();
        Bundle args = new Bundle();
        args.putSerializable("chosenOne", recipe);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            RecipeHolder tmp = (RecipeHolder) getArguments().getSerializable("chosenOne");
            assert tmp != null;
            //Get the meal
            chosenOne = tmp.getMeal();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_show_meal, container, false);
        //Set the recipe Image and nr of portions / recipe
        setUpImageAndMore(v);
        //Set up the macros list
        setUpMacros(v);
        //Set up the ingredients
        setUpIngredientList(v);
        //Set up the instructions for the recipe
        setUpInstructions(v);
        return v;
    }
    private void setUpImageAndMore(View v) {
        //Set the title
        TextView title = v.findViewById(R.id.meal_title);
        title.setText(chosenOne.get("title").toString());
        //Get the imageView and get the img String
        ImageView imageView = v.findViewById(R.id.mImage);
        String imageUrl = (String) chosenOne.get("ImageUrl");
        //Set the image with Glide
        Glide.with(imageView.getContext())
                .load(imageUrl)
                .into(imageView);

        //Amount of portions.
        TextView portions = v.findViewById(R.id.nr_port);
        HashMap<String, Object> tmp = (HashMap<String, Object>) chosenOne.get("ingredientList");
        portions.setText(tmp.get("Portions").toString());
    }
    private void setUpMacros(View v) {
        //Get the layout
        LinearLayout parent = v.findViewById(R.id.parent_meal);
        //Create the title
        TextView macros = new TextView(getContext());
        //Set configuration
        macros.setText(R.string.macrosPer);
        macros.setGravity(Gravity.CENTER);
        Typeface typeface = ResourcesCompat.getFont(requireContext(), R.font.proxima);
        macros.setTextColor(ContextCompat.getColor(requireContext(), R.color.white));
        macros.setTypeface(typeface);
        macros.setTextSize(20);
        //Add
        parent.addView(macros);

        //Get the macro entry.
        HashMap<String, Object> macroList = (HashMap<String, Object>) chosenOne.get("nutritionPerPortion");
        //For every entry.
        for (Map.Entry<String, Object> entry : macroList.entrySet()) {
            String key = entry.getKey();
            Object value = entry.getValue();
            //the key is ex "Protein" and the value is the amount.
            TextView macroTextView = new TextView(getContext());
            //Set the text and configure it.
            macroTextView.setText(key + ": " + value.toString());
            macroTextView.setTextColor(ContextCompat.getColor(requireContext(), R.color.white));
            macroTextView.setTypeface(typeface);
            //Add the element to the parent.
            parent.addView(macroTextView);
        }
    }
    private void setUpInstructions(View v) {
        //Get the parent layout
        LinearLayout parent = v.findViewById(R.id.parent_meal);
        //Create the title and configure
        TextView instructions = new TextView(getContext());
        Typeface typeface = ResourcesCompat.getFont(requireContext(), R.font.proxima);
        instructions.setText(R.string.instructions_text);
        instructions.setGravity(Gravity.CENTER);
        instructions.setTextColor(ContextCompat.getColor(requireContext(), R.color.white));
        instructions.setTypeface(typeface);
        instructions.setTextSize(20);
        //Create layout params for the title.
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        layoutParams.topMargin = 16;
        //set the params and add to parent.
        instructions.setLayoutParams(layoutParams);
        parent.addView(instructions);
        //get the steps and for every step
        //Create a TextView, configure it and add to parent.
        ArrayList<String> steps = (ArrayList<String>) chosenOne.get("steps");
        for (String step : steps) {
            TextView tmp = new TextView(getContext());
            tmp.setText(step);
            tmp.setTextColor(ContextCompat.getColor(requireContext(), R.color.white));
            tmp.setTypeface(typeface);
            parent.addView(tmp);

        }
    }



    private void setUpIngredientList(View v) {
        //Set up the ingredients list.
        //Init interface
        Typeface typeface = ResourcesCompat.getFont(requireContext(), R.font.proxima);
        //Get the parent layout
        LinearLayout parent = v.findViewById(R.id.parent_meal);
        //Create the title and configure it.
        TextView ingredientsText = new TextView(getContext());
        ingredientsText.setText(R.string.ingredients);
        ingredientsText.setGravity(Gravity.CENTER);
        ingredientsText.setTextColor(ContextCompat.getColor(requireContext(), R.color.white));
        ingredientsText.setTypeface(typeface);
        ingredientsText.setTextSize(20);
        //Create layoutParams
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        //Set the margin
        layoutParams.topMargin = 16;
        //Add the params to the text
        ingredientsText.setLayoutParams(layoutParams);
        //Add to the parent
        parent.addView(ingredientsText);

        //Get the list
        HashMap<String, Object> hashMap = (HashMap<String, Object>) chosenOne.get("ingredientList");
        ArrayList<HashMap<String, Object>> ingredients = (ArrayList<HashMap<String, Object>>) hashMap.get("Ingredients");

        assert ingredients != null;
        //For ingredient map
        for (HashMap<String, Object> ingredient : ingredients) {
            TextView tmp = new TextView(getContext());
            //Get the text key value "Text"
            tmp.setText(Objects.requireNonNull(ingredient.get("Text")).toString());
            tmp.setTextColor(ContextCompat.getColor(requireContext(), R.color.white));
            tmp.setTypeface(typeface);
            parent.addView(tmp);

        }
    }

}