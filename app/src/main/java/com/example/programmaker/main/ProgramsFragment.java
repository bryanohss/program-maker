package com.example.programmaker.main;

import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import androidx.core.content.ContextCompat;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.programmaker.R;

import java.util.ArrayList;
import java.util.Objects;
//Shows the list of saved programs.
public class ProgramsFragment extends Fragment {

    private ArrayList<String> myWorkouts;
    public ProgramsFragment() {
        // Required empty public constructor
    }

    public static ProgramsFragment newInstance(ArrayList<String> programs) {
        //Put programs in args
        ProgramsFragment fragment = new ProgramsFragment();
        Bundle args = new Bundle();
        args.putStringArrayList("programs", programs);
        fragment.setArguments(args);

        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //init MyWorkouts
            myWorkouts = getArguments().getStringArrayList("programs");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_programs, container, false);
        //If myWorkouts is empty set up a text, if we have programs set up the list.
        if(!myWorkouts.isEmpty()){
            SetWorkoutList(v);
        } else{
            setUpEmptyText(v);
        }
        //Set upp import button.
        setUpImportBtn(v);
        return v;
    }

    private void setUpEmptyText(View v) {
        //Create a new TextView and set required configuration.
        TextView text = new TextView(requireContext());
        text.setText(R.string.empty);
        text.setTextColor(ContextCompat.getColor(requireContext(), R.color.white));
        text.setTextSize(20);

        ConstraintLayout.LayoutParams layoutParams = new ConstraintLayout.LayoutParams(
                ConstraintLayout.LayoutParams.WRAP_CONTENT,
                ConstraintLayout.LayoutParams.WRAP_CONTENT
        );

        layoutParams.startToStart = ConstraintLayout.LayoutParams.PARENT_ID;
        layoutParams.endToEnd = ConstraintLayout.LayoutParams.PARENT_ID;
        layoutParams.topToTop = ConstraintLayout.LayoutParams.PARENT_ID;
        layoutParams.bottomToBottom = ConstraintLayout.LayoutParams.PARENT_ID;

        text.setLayoutParams(layoutParams);
        text.setGravity(Gravity.CENTER);
        //Get the parent layout and add the text.
        ConstraintLayout layout = v.findViewById(R.id.program_layout);
        layout.addView(text);

    }

    private void SetWorkoutList(View v) {
        //Get the listView
        ListView listView = v.findViewById(R.id.programs_lst);
        //Create and ArrayAdapter with custom layout and the list.
        ArrayAdapter<String> adapter = new ArrayAdapter<>(requireContext(),
                R.layout.list_item_white_text, myWorkouts);
        //Set the adapter
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Log.i("hejsan", myWorkouts.get(i));
                ((NavMain)requireActivity()).showProgram(myWorkouts.get(i));
            }
        });
    }
    private void setUpImportBtn(View v) {
        //get the button and set the setOnClickListener.
        Button importBtn = v.findViewById(R.id.import_btn);
        importBtn.setOnClickListener(view -> importW());
    }

    private void importW() {
        //Create a view with custom layout.
        View dialogView = LayoutInflater.from(requireContext()).inflate(R.layout.import_layout, null);
        //Get the builder.
        AlertDialog.Builder builder = new AlertDialog.Builder(requireContext());
        //Set the view, add the title and create.
        builder.setView(dialogView);
        builder.setTitle(R.string.import_text2);
        AlertDialog alertDialog = builder.create();
        //Get the import button from dialogView.
        //Set the clickListener.
        Button importBtn = dialogView.findViewById(R.id.import_btn2);
        importBtn.setOnClickListener(view -> saveWorkout(dialogView));
        //Show.
        alertDialog.show();
    }

    private void saveWorkout(View dialogView) {
        //Get the code from input
        //Call method in NavMain
        EditText codeET = dialogView.findViewById(R.id.code_input);
        ((NavMain)requireActivity()).importWorkout(codeET.getText().toString());
    }
}