package com.example.programmaker.main;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.programmaker.R;
import com.example.programmaker.adapters.MealAdapter;
import com.example.programmaker.customC.RecipeHolder;

import java.util.HashMap;
import java.util.List;

//Shows the Meals available

public class DietFragment extends Fragment implements MealAdapter.MealAdapterListener {


   private List<HashMap<String, Object>> recipeList; //List of every recipe
   private List<HashMap<String, Object>> forMe; //List of recipes for me


    public DietFragment() {
        // Required empty public constructor
    }

    //The use of RecipeHolder is only for send arguments to fragments.
    public static DietFragment newInstance(RecipeHolder recipesList, RecipeHolder forMeHolder) {
        DietFragment fragment = new DietFragment();
        Bundle args = new Bundle();
        args.putSerializable("list", recipesList); //Put the all recipes in args
        args.putSerializable("forMe", forMeHolder); //Put the forMe in args
        fragment.setArguments(args);
        return fragment;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            RecipeHolder tmp = (RecipeHolder) getArguments().getSerializable("list");
            RecipeHolder tmp2 = (RecipeHolder) getArguments().getSerializable("forMe");
            assert tmp != null;
            assert tmp2 != null;

            recipeList = tmp.getRecipeList(); //Init all recipes
            forMe = tmp2.getRecipeList(); //Init For me list

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_diet, container, false);
        //Set up the lists.
        setUpForMeLst(v);
        setUpAllList(v);

        return v;
    }

    private void setUpAllList(View v) {
        //Get the recycler view
        RecyclerView lst = v.findViewById(R.id.all_meals_lst);
        //Set the layoutManager
        lst.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false));
        //Create the adapter and set.
        MealAdapter mealAdapter = new MealAdapter(recipeList, this);
        lst.setAdapter(mealAdapter);
    }

    private void setUpForMeLst(View v) {
        //Get the recycler view
        RecyclerView lst = v.findViewById(R.id.for_me_lst);
        //Set the layoutManager
        lst.setLayoutManager(new LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false));
        //Create the adapter and set.
        MealAdapter mealAdapter = new MealAdapter(forMe, this);
        lst.setAdapter(mealAdapter);
    }

    @Override
    public void onMealClicked(HashMap<String, Object> clicked) {
        //The onClick method call method on NavMain
        RecipeHolder recipeHolder = new RecipeHolder(clicked);
        ((NavMain)requireActivity()).showMeal(recipeHolder);



    }
}