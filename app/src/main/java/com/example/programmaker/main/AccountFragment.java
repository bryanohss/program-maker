package com.example.programmaker.main;

import android.app.AlertDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.example.programmaker.R;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
//Shows settings of the account.
public class AccountFragment extends Fragment {

    private String username;
    private String email;
    private Long gender;
    private Long dgoals;
    private Long tgoals;

    private TextView gText;
    private TextView dText;
    private TextView tText;

    private String[] genderArray;
    private String[] dGoalsArray;
    private String[] tGoalsArray;

    public AccountFragment() {
        // Required empty public constructor
    }


    public static AccountFragment newInstance(Map<String, Object> data) {
        AccountFragment fragment = new AccountFragment();
        Bundle args = new Bundle();
        //data is userData.
        Log.i("data", data.toString());
        args.putString("username", (String) data.get("username"));
        args.putString("email", (String) data.get("email"));
        args.putLong("gender", (Long) data.get("gender"));
        args.putLong("dgoals", (Long) data.get("dGoals"));
        args.putLong("tgoals", (Long) data.get("tGoals"));
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            //Init all the variables
            username = getArguments().getString("username");
            email = getArguments().getString("email");
            gender = getArguments().getLong("gender");
            dgoals = getArguments().getLong("dgoals");
            tgoals = getArguments().getLong("tgoals");
        }
        //Init the array.
        genderArray = getResources().getStringArray(R.array.gender_options);
        dGoalsArray = getResources().getStringArray(R.array.diet_options);
        tGoalsArray = getResources().getStringArray(R.array.goals_options);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_account, container, false);
        //Get the signOut Button and set the clickListener.
        Button signOut = v.findViewById(R.id.sign_out_btn);
        signOut.setOnClickListener(view -> signOutUser());
        //calling setup functions with view
        setUpPreferences(v);
        setUpClickListeners(v);
        setUpLanguageSpinner(v);
        return v;
    }

    private void setUpClickListeners(View v) {
        //Get the linearLayouts and set clickListeners.
        LinearLayout genderL = v.findViewById(R.id.gender_layout);
        //Set the parameter to gender to get the correct array of options.
        genderL.setOnClickListener(view -> changePreference("gender"));
        LinearLayout tGoalsL = v.findViewById(R.id.workout_layout);
        //Set the parameter to tGoals to get the correct array of options.
        tGoalsL.setOnClickListener(view -> changePreference("tGoals"));
        LinearLayout dGoalsL = v.findViewById(R.id.diet_layout);
        //Set the parameter to dGoals to get the correct array of options.
        dGoalsL.setOnClickListener(view -> changePreference("dGoals"));

    }

    private void changePreference(String preference) {
        //https://developer.android.com/develop/ui/views/components/dialogs
        //Create a dialogView
        View dialogView = LayoutInflater.from(getContext()).inflate(R.layout.preference_change_layout, null);
        //init the builder
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        //set the view
        builder.setView(dialogView);
        //create the AlertDialog
        AlertDialog alertDialog = builder.create();
        //Get the spinner
        Spinner spinner = dialogView.findViewById(R.id.preference_spinner);
        ArrayAdapter<CharSequence> adapter = null;
        //selected string array.
        String[] selected = new String[0];
        switch (preference) { //init the adapter with different depending on the preference selected
            case "tGoals":
                adapter = ArrayAdapter.createFromResource(requireContext(), R.array.goals_options, android.R.layout.simple_spinner_item);
                selected = tGoalsArray;
                break;
            case "gender":
                adapter = ArrayAdapter.createFromResource(requireContext(), R.array.gender_options, android.R.layout.simple_spinner_item);
                selected = genderArray;
                break;
            case "dGoals":
                adapter = ArrayAdapter.createFromResource(requireContext(), R.array.diet_options, android.R.layout.simple_spinner_item);
                selected = dGoalsArray;
                break;
        }
        //set the spinner
        spinner.setAdapter(adapter);
        //get the changeButton, assign a finalSelected.
        Button changeButton = dialogView.findViewById(R.id.changeButton);
        //Final selected is the array with options.
        String[] finalSelected = selected;
        //Set the ClickListener.
        changeButton.setOnClickListener(v ->  changePrefClick(spinner, finalSelected, preference, alertDialog));

        // Show the dialog
        alertDialog.show();
    }

    private void changePrefClick(Spinner spinner, String[] finalSelected, String preference, AlertDialog alertDialog) {
        //selectedOption is the selected item from the spinner
        String selectedOption = (String) spinner.getSelectedItem();
        //call the updatePreferences in NavMain
        ((NavMain)requireActivity()).updatePreferences(preference, selectedOption, finalSelected);
        //Update the correct UI text.
        if(Arrays.equals(finalSelected, tGoalsArray)){
            tText.setText(selectedOption);
        } else if(Arrays.equals(finalSelected, dGoalsArray)){
            dText.setText(selectedOption);
        } else if (Arrays.equals(finalSelected, genderArray)) {
            gText.setText(selectedOption);
        }
        //Dismiss Dialog.
        alertDialog.dismiss();
    }


    private void setUpPreferences(View v) {
        //Get textViews and set the text.
        TextView uText = v.findViewById(R.id.user_text);
        TextView eText = v.findViewById(R.id.email_text);
        uText.setText(username);
        eText.setText(email);

        gText = v.findViewById(R.id.gender_text);
        dText = v.findViewById(R.id.dgoals_text);
        tText = v.findViewById(R.id.tgoals_text);

        gText.setText(genderArray[gender.intValue()]);
        dText.setText(dGoalsArray[dgoals.intValue()]);
        tText.setText(tGoalsArray[tgoals.intValue()]);
    }

    private void setUpLanguageSpinner(View v) {
        //Configure the spinner.
        Spinner spinner = v.findViewById(R.id.languageSpinner);
        //Create the adapter with custom Array.
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(requireContext(), R.array.language_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter); //set the adapter.
        //Get the current locale.
        String currentLocaleString = Locale.getDefault().toString();
        int selectedIndex;
        //Only two options. "sv" and "eng".
        if (currentLocaleString.equals("sv")) {
            selectedIndex = 1;
        } else {
            selectedIndex = 0;
        }
        //Set the current localte to selected.
        spinner.setSelection(selectedIndex);
        //Set the on item Selected.
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                String selection = adapterView.getItemAtPosition(pos).toString();
                //create sets for possible "selection".
                Set<String> se = new HashSet<>(Arrays.asList("Swedish", "Svenska"));
                Set<String> eng = new HashSet<>(Arrays.asList("English", "Engelska"));

                // Check if the selected language is in the set of supported languages
                if (se.contains(selection)) {
                    changeLan("sv");
                } else if (eng.contains(selection)) {
                    changeLan("en");
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }


    private void changeLan(String lan) {
        //Call changeLan on NavMain
        ((NavMain)requireActivity()).changeLan(lan);
    }

    private void signOutUser(){
        //Call signOutUser on NavMain
        ((NavMain)requireActivity()).singOutUser();
    }
}