package com.example.programmaker.main;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.LocaleList;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.example.programmaker.programmanager.ProgramManager;
import com.example.programmaker.R;
import com.example.programmaker.auth.MainActivity;
import com.example.programmaker.createprogram.CreateProgram;
import com.example.programmaker.customC.LanguageManager;
import com.example.programmaker.customC.RecipeHolder;
import com.example.programmaker.customC.WorkoutPlan;
import com.example.programmaker.databinding.ActivityNavMainBinding;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.firebase.firestore.SetOptions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

//This class is the mainApp
//Biggest class.
public class NavMain extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private FirebaseUser currentUser;
    private LanguageManager languageManager;
    private AccountFragment accFragment;
    private CreateFragment crFragment;
    private Map<String, Object> userData;
    private ProgramsFragment myProgramsFrag;
    private View loadingContainer;
    private DocumentReference userRef;
    private ArrayList<HashMap<String, Object>> workoutsData;
    private HashMap<String, Object> logData;
    private List<WorkoutPlan> allPrograms;
    private List<WorkoutPlan> forMe;
    private List<HashMap<String, Object>> recipesList;
    private List<HashMap<String, Object>> recipesMe;


    private boolean stateExercises = false;
    private boolean stateRecipes = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Init the lists.
        allPrograms = new ArrayList<>();
        forMe = new ArrayList<>();
        recipesList = new ArrayList<>();
        recipesMe = new ArrayList<>();
        // Inflate the layout using data binding.
        // The generated binding class is used to access views defined in the layout XML.
        com.example.programmaker.databinding.ActivityNavMainBinding binding = ActivityNavMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        //Show the createFragment as default.
        binding.bottomNav.getMenu().findItem(R.id.create_menu).setChecked(true);
        crFragment = CreateFragment.newInstance();
        replaceFragment(crFragment); //call method replaceFragment.

        //Configure the BottomNav
        binding.bottomNav.setOnItemSelectedListener(item -> {
            if (item.getItemId() == R.id.diet_menu) {
                //Diet menu. list of recipes based on the users preferences
                //Init the lists and send them to the fragment
                RecipeHolder recipeHolder = new RecipeHolder(recipesList);
                RecipeHolder forMeHolder = new RecipeHolder(recipesMe);
                DietFragment dietFragment = DietFragment.newInstance(recipeHolder, forMeHolder);
                replaceFragment(dietFragment);

            } else if (item.getItemId() == R.id.explore_menu) {
                //Init the fragment with lists and sent them
                //Explore menu. list of workoutPlans based on the users preferences
                ExploreFragment exploreFragment = ExploreFragment.newInstance(forMe, allPrograms);
                replaceFragment(exploreFragment);
            } else if (item.getItemId() == R.id.create_menu) {
                //Change the fragment
                //Start creating your workoutPlan
                crFragment = CreateFragment.newInstance();
                replaceFragment(crFragment);
            } else if (item.getItemId() == R.id.menu_menu) {
                //This fragment shows the user's workoutsPlans
                //workoutsData a list with the user's workoutPlans
                ArrayList<String> lst = new ArrayList<>();
                if(workoutsData != null){
                    //Get the names of the workoutPlans
                    for (HashMap<String, Object> map : workoutsData) {
                        lst.add(Objects.requireNonNull(map.get("name")).toString());
                    }
                }
                //Init the fragment and replace it.
                myProgramsFrag = ProgramsFragment.newInstance(lst);
                replaceFragment(myProgramsFrag);

            } else if (item.getItemId() == R.id.account_menu) {
                //Account menu, shows the users preferences, username and email.
                //Init the fragment with the users data and replace it.
                accFragment = AccountFragment.newInstance(userData);
                replaceFragment(accFragment);
            }
            return true;
        });

    }




    public void createActivity() {
        //Create the intent and start it.
        Intent in = new Intent(this, CreateProgram.class);
        startActivity(in);
    }

    private void replaceFragment(Fragment fragment){
        //Replace the current fragment with the given one.
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_place, fragment).commit();
    }


    public void singOutUser() {
        //Sign out User and redirect them to the mainActivity.
        mAuth.signOut();
        Intent in = new Intent(this, MainActivity.class);
        startActivity(in);
    }

    //Get the data of the user
    private void getCurrentUserData() {
        //Get the document reference
        DocumentReference userRef = db.collection("users").document(currentUser.getUid());
        //Set a listener
        //Every Time a change is made
        //this method gets called.
        userRef.addSnapshotListener((value, error) -> {
            if(error != null){
                Log.w("error", error);
                return ;
            }
            assert value != null;
            if(value.exists()){
                userData = value.getData();
                assert userData != null;
                workoutsData = (ArrayList<HashMap<String, Object>>) userData.get("myWorkouts");
                logData = (HashMap<String, Object>) userData.get("workoutLog");
                //This gets called here because we need the users preferences.
                getPrograms(); // Call get programs
                getRecipes(); //Call get recipes
            }
        });
    }

    public void showProgram(String programName) {
        //Get the chosenProgram to show
        //Loop and compare the name with the given string.
        HashMap<String, Object> toShow = new HashMap<>();
        for (HashMap<String, Object> map : workoutsData) {
            if(Objects.equals(map.get("name"), programName)){
                toShow = map;
                break; //Break when matched
            }
        }
        //Create the intent and put the program, the logData and false in buttons
        //this booleans is used to display certain buttons
        Intent in = new Intent(this, ProgramManager.class);
        in.putExtra("program", toShow);
        in.putExtra("logData", logData);
        in.putExtra("buttons", false);
        startActivity(in);
    }

    private void showLoadingScreen() {
        //This functions shows the loading screen.
        //This because the UI is not ready until we make all the call to FireStore
        loadingContainer = findViewById(R.id.loading_container);
        loadingContainer.setVisibility(View.VISIBLE);
    }

    //hides the loading
    //App ready to use
    private void hideLoadingScreen() {
        loadingContainer.setVisibility(View.GONE);
    }


    public void updatePreferences(String preference, String selectedOption, String[] finalSelected) {
        //Create new Hashmap to save the new preference
        Map<String, Object> newPreference = new HashMap<>();
        //To be able to display the preference in more than 1 language
        //I use the index of the preference.
        //In the loop we get the index of the selectedOption.
        int selectedIndex = -1;
        for(int i = 0; i < finalSelected.length; i++){
            if(finalSelected[i].equals(selectedOption)){
                selectedIndex = i;
            }
        }
        //in newPreference put the preference that is being changed (ex. gender)
        //and the index (ex, 2)
        newPreference.put(preference, selectedIndex);
        //We merge the existing and the new.
        //return feedback to the user
        userRef.set(newPreference, SetOptions.merge()).addOnSuccessListener(unused -> Toast.makeText(NavMain.this, "Preference Updated.",
                Toast.LENGTH_SHORT).show());
    }

    public void changeLan(String lan) {
        //Get the configuration, and the list of locales
        Configuration currentConfig = getResources().getConfiguration();
        LocaleList currentLocales = currentConfig.getLocales();
        //Get the current locale
        String currentLanguage = currentLocales.get(0).getLanguage();
        //If the given language is different from the current, change and recreate.
        if (!lan.equals(currentLanguage)) {
            languageManager.changeLanguage(this, lan);
            recreate();
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        //On start
        //Init variables
        //show loading screen
        //and call get current user data
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        currentUser = mAuth.getCurrentUser();
        if(currentUser == null){
            redirectToLogin();
        }
        userRef = db.collection("users").document(currentUser.getUid());
        languageManager.loadSavedLocalePreferences(this);
        showLoadingScreen();
        getCurrentUserData();
    }

    private void redirectToLogin() {
        Intent in = new Intent(this, MainActivity.class);
        startActivity(in);
    }

    private void getRecipes() {
        //Get the CollectionReference and get everything.
        CollectionReference collectionReference = db.collection("recipes");
        collectionReference.get()
                .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                    @Override
                    public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                        //we don't want duplicates so we clear them.
                        recipesMe.clear();
                        recipesList.clear();
                        //For every Document (recipe) cast it to a HashMap.
                        for(QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                            HashMap<String, Object> tmp = (HashMap<String, Object>) documentSnapshot.getData();
                            //If the recipe "for" attribute is equal to our dGoals (diet goals) add them to
                            //recipesMe. Add all recipes to "recipesList"
                            if(userData.get("dGoals") == tmp.get("for")){
                                recipesMe.add(tmp);
                            }

                            recipesList.add(tmp);
                        }
                        //set the recipes State as true and call checkStates
                        stateRecipes = true;
                        checkStates();

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //On failure Log it.
                        Log.i("failure", "Error" + e.toString());
                    }
                });

    }

    private void getPrograms() {
        //Get the collectionReference and get every document.
        CollectionReference collectionReference = db.collection("programs");
        collectionReference.get()
                        .addOnSuccessListener(new OnSuccessListener<QuerySnapshot>() {
                            @Override
                            public void onSuccess(QuerySnapshot queryDocumentSnapshots) {
                                //we don't want duplicates so we clear them.
                                forMe.clear();
                                allPrograms.clear();
                                //For every document cast it to a Hashmap and init a new WorkoutPlan
                                //If the focus of the program equals our tGoals (training goals) add them to "forMe"
                                //every Program is added to allProgram
                                for(QueryDocumentSnapshot documentSnapshot : queryDocumentSnapshots){
                                    WorkoutPlan tmp = new WorkoutPlan((HashMap<String, Object>) documentSnapshot.getData());
                                    if(userData.get("tGoals") == tmp.getFocus()){
                                        forMe.add(tmp);
                                    }
                                    allPrograms.add(tmp);
                                }
                                //Update the state
                                //call checkState
                                stateExercises = true;
                                checkStates();


                            }
                        }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //On failure
                        Log.i("failure", "Error" + e.toString());
                    }
                });
    }

    private void checkStates() {
        //If we have loaded the recipes and the workouts, dismiss the loading screen
        if(stateExercises && stateRecipes ){
            hideLoadingScreen();
        }
    }

    //Before on Create.
    //Get called when the base Context in being attached.
    @Override
    protected void attachBaseContext(Context newBase) {
        languageManager = new LanguageManager(newBase); //Init languageManager with the context.
        super.attachBaseContext(languageManager.setLocale(newBase)); //LanguaManager setLocale returns a context with the configuration of current Locale
    }

    //This fuction imports a workoutPlan from a code.
    public void importWorkout(String code) {
        //The first part of the code is UID of the user that shared the program and the second part
        //is the name of the workout they shared.
        String[] codeTemplate = code.split("-");
        //Get the document reference with the first part of the string.
        DocumentReference userRef = db.collection("users").document(codeTemplate[0]);
        userRef.get().addOnSuccessListener(documentSnapshot -> {
            //If we get a match.
            if (documentSnapshot.exists()) {
                //Get the name of the user.
                String name = Objects.requireNonNull(documentSnapshot.get("username")).toString();
                //Get the hashmap containing all the workouts.
                List<Map<String, Object>> myWorkouts = (List<Map<String, Object>>) documentSnapshot.get("myWorkouts");
                if (myWorkouts != null) {
                    //Loop and see what map has the name that equals the second part of the code string.
                    for (Map<String, Object> workout : myWorkouts) {
                        if (codeTemplate[1].equals(Objects.requireNonNull(workout.get("name")).toString())) {
                            //oldValue is the name of the workout
                            String oldValue = (String) workout.get("name");
                            //new value is the new name for the workout. the name of the username + the name of the workout.
                            String newValue = name + "-" + oldValue;
                            //We replace it .
                            workout.replace("name", newValue);
                            //Get the document reference on the current user.
                            DocumentReference currentUserRef = db.collection("users").document(currentUser.getUid());
                            //We update the current user's data with the new workout.
                            //Use FieldValue.arrayUnion to append to the existent array.
                            currentUserRef.update("myWorkouts", FieldValue.arrayUnion(workout))
                                    //On Success and Failure go back and give the user some feedback.
                                    .addOnSuccessListener(aVoid -> {
                                        onBackPressed();
                                        Log.i("newValue", "Workout added to myWorkouts successfully");
                                    })
                                    .addOnFailureListener(e -> {
                                        onBackPressed();
                                        Log.e("TAG", "Error adding workout to myWorkouts", e);
                                    });
                            break; // Break the loop.
                        }
                    }
                }
            } else {
                Log.d("TAG", "Workout not exist");
            }
        }).addOnFailureListener(e -> Log.e("FireStore Error", "Error", e));
    }


    public void showCaseProgram(WorkoutPlan chosenOne) {
        //Start the intent with the ProgramManager class
        //send the chosen program. call toMap.
        //send the logData.
        //send boolean true, this shows other set of buttons.
        Intent in = new Intent(this, ProgramManager.class);
        in.putExtra("program", chosenOne.toMap());
        in.putExtra("logData", logData);
        in.putExtra("buttons", true);
        startActivity(in);
    }

    public void showMeal(RecipeHolder recipeHolder) {
        //Start fragment with the recipeHolder and replace it.
        ShowMealFragment showMealFragment = ShowMealFragment.newInstance(recipeHolder);
        replaceFragment(showMealFragment);
    }
}
