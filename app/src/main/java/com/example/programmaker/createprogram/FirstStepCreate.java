package com.example.programmaker.createprogram;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.example.programmaker.R;
//This is the first step in creation.
public class FirstStepCreate extends Fragment {


    public FirstStepCreate() {
        // Required empty public constructor
    }


    public static FirstStepCreate newInstance() {
        return new FirstStepCreate();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_first_step_create, container, false);
        //Set up the button and listener.
        Button nextBtn = v.findViewById(R.id.next_btn);
        nextBtn.setOnClickListener(view -> setWorkoutName(v));
        
        return v;
    }

    private void setWorkoutName(View v) {
        //Get the editText and set it as parameter to setWorkoutName func.
        EditText name = v.findViewById(R.id.wName);
        ((CreateProgram)requireActivity()).setWorkoutName(name.getText().toString());
    }
}