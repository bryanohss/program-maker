package com.example.programmaker.createprogram;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

import com.example.programmaker.R;
//Second Step.

public class SecondStepCreate extends Fragment {


    public SecondStepCreate() {
        // Required empty public constructor
    }


    public static SecondStepCreate newInstance() {
        return new SecondStepCreate();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_second_step_create, container, false);
        //Get the button and set the clickListener
        Button nextBtn = v.findViewById(R.id.next_btn);
        nextBtn.setOnClickListener(view -> createWorkout(v));
        return v;
    }

    private void createWorkout(View v) {
        //Get the text from the editText and send it as parameter to startCreateWorkout.
        EditText workoutName = v.findViewById(R.id.work_name);
        ((CreateProgram)requireActivity()).startCreateWorkout(workoutName.getText().toString());
    }
}