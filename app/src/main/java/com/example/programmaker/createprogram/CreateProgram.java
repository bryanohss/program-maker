package com.example.programmaker.createprogram;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.programmaker.programmanager.EditWorkout;
import com.example.programmaker.R;
import com.example.programmaker.customC.Exercise;
import com.example.programmaker.customC.LanguageManager;
import com.example.programmaker.customC.WorkoutPlan;
import com.example.programmaker.main.NavMain;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

//Create Activity.
//This activity is responsible for the creation of new programs.
public class CreateProgram extends AppCompatActivity implements EditWorkout.EditWorkoutListener, CreateWorkout.AddExerciseListener {
    private LanguageManager languageManager;
    private WorkoutPlan wPlan;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private List<Exercise> exercises;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_program);
        //Init db, wPlan and mAuth.
        db = FirebaseFirestore.getInstance();
        wPlan = new WorkoutPlan(); //WorkoutPlan will contain all the workouts.
        mAuth = FirebaseAuth.getInstance();

        //Init first step fragment.
        FirstStepCreate firstFragment = FirstStepCreate.newInstance();
        //callback function.
        //I dont want to show the fragment until we have the list of exercises.
        getExercises(lst -> {
            exercises = lst;
            Log.i("get list", exercises.toString());
            Log.i("info", exercises.get(0).getInfo().toString());
            getSupportFragmentManager().beginTransaction().add(R.id.createLayout, firstFragment).commit();
        });

    }

    public void setWorkoutName(String name) {
        //Set the name of the program
        //If empty, return feedback to the user.
        if(!name.equals("")){
            wPlan.setName(name);
            //get to the second step.
            SecondStepCreate secFragment = new SecondStepCreate();
            getSupportFragmentManager().beginTransaction().replace(R.id.createLayout, secFragment).commit();
        } else {
            Toast.makeText(CreateProgram.this, "Please enter a program name",
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void startCreateWorkout(String name) {
        //If the name is not empty.
        if(!name.equals("")) {
            //Next Step of the process.
            //The name of the workout is valid.
            CreateWorkout cWorkout = CreateWorkout.newInstance(name, exercises);
            getSupportFragmentManager().beginTransaction().replace(R.id.createLayout, cWorkout).commit();
        } else{
            //If empty return feedback to the user
            Toast.makeText(CreateProgram.this, "Please enter a workout name",
                    Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void addWorkoutToPlan(String workoutName, List<Exercise> selectedExersices) {
        //Here we add the workout!
        //Time to start editing
        wPlan.addWorkout(workoutName, selectedExersices);
        startEditWorkout(workoutName);
    }

    protected void startEditWorkout(String name) {
        //Init the editWorkout fragment and show it.
        EditWorkout editWorkout = EditWorkout.newInstance(name, wPlan);
        getSupportFragmentManager().beginTransaction().replace(R.id.createLayout, editWorkout).commit();
    }

    public void saveWorkoutPlan() {
        //Save the workoutPlan to your plans.
        DocumentReference userRef = db.collection("users").document(Objects.requireNonNull(mAuth.getCurrentUser()).getUid());
        //We dont want to overwrite. FieldValue.ArrayUnion fixes this.
        userRef.update("myWorkouts", FieldValue.arrayUnion(wPlan))
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        //If Successful return feedback
                        Toast.makeText(CreateProgram.this, "Program Saved",
                                Toast.LENGTH_SHORT).show();

                        createLog(); //create the log

                        redirectToMain(); //redirect to main activity.
                    }

                    //https://cloud.google.com/firestore/docs/samples/firestore-data-set-nested-fields#firestore_data_set_nested_fields-java
                    private void createLog() {
                        //Empty List
                        //Here we use Nested-Fields to update the correct database field.
                        List<Object> empty = new ArrayList<>();
                        String workoutPlanName = wPlan.getName();
                        Map<String, Object> updateData = new HashMap<>();
                        //in field workoutLog, key workoutName set the empty Array.
                        updateData.put("workoutLog." + workoutPlanName, empty);

                        userRef.update(updateData);
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    //If failure return feedback
                    public void onFailure(@NonNull Exception e) {
                        Toast.makeText(CreateProgram.this, "Could not save Workout!, Please try again later",
                                Toast.LENGTH_SHORT).show();
                    }
                });

    }

    @Override
    public void setUpButtons(View v, String workoutName) {
        //Set up buttons and listeners.
        Button savePlan = v.findViewById(R.id.save_workout);
        savePlan.setOnClickListener(view -> saveWorkoutPlan());

        Button anotherButton = v.findViewById(R.id.add_another);
        anotherButton.setOnClickListener(view -> addAnotherWorkout(workoutName));
    }

    private void redirectToMain() {
        //Create indent and start it.
        Intent in = new Intent(this, NavMain.class);
        startActivity(in);
    }
    @Override
    public void addAnotherWorkout(String workoutName) {
        //If the users wants to add more
        //redirect them to secondStep again.
        SecondStepCreate secFragment = new SecondStepCreate();
        getSupportFragmentManager().beginTransaction().replace(R.id.createLayout, secFragment).commit();
    }


    //Callbacks https://www.onespan.com/blog/callback-methods-overview
    //Interface for callbacks.
    public interface OnExercisesLoadedListener {
        void onExercisesLoaded(List<Exercise> exercises);
    }

    public void getExercises(final OnExercisesLoadedListener listener) {
        //Get the collection reference
        CollectionReference exercisesRef = db.collection("exercises");
        //get all the exercises.
        exercisesRef.get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    //Create an empty list.
                    //Cast every queryDocumentSnapshop to Exercise Class and add them to result.
                    List<Exercise> result = new ArrayList<>();
                    for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                        Exercise tmp = doc.toObject(Exercise.class);
                        result.add(tmp);
                    }
                    listener.onExercisesLoaded(result); //call the callback.
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i("ERROR", e.toString());
                    } //Log if failure
                });
    }

    //Before on Create.
    //Get called when the base Context in being attached.
    @Override
    protected void attachBaseContext(Context newBase) {
        languageManager = new LanguageManager(newBase); //Init languageManager with the context.
        super.attachBaseContext(languageManager.setLocale(newBase)); //LanguageManager setLocale returns a context with the configuration of current Locale
    }

    @Override
    public void onStart() {
        super.onStart();
        //Load Saved Preferences.
        //This will load the locale in SharedPreferences.
        //Adhd update the resources if there is a saved Locale.
        languageManager.loadSavedLocalePreferences(this);

    }
}
