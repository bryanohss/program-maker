package com.example.programmaker.createprogram;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SearchView;

import androidx.fragment.app.Fragment;

import com.example.programmaker.R;
import com.example.programmaker.adapters.CustomStringAdapter;
import com.example.programmaker.customC.Exercise;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
//This fragment is where the users select the exercises for their workout.
public class CreateWorkout extends Fragment {

    private String workoutName;
    private List<Exercise> selectedExercises;
    private List<Exercise> exerciseList;
    private Map<String, Object> nameList;
    private AddExerciseListener addExerciseListener;

    public CreateWorkout() {
    }
    //Implements an interface
    //So multiple activities can use this fragment.
    public interface AddExerciseListener{
        void addWorkoutToPlan(String workoutName, List<Exercise> selectedExersices);
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // Check if the hosting activity implements the AddExerciseListener interface.
        if (context instanceof AddExerciseListener) {
            addExerciseListener = (CreateWorkout.AddExerciseListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement EditWorkoutListener");
        }
    }

    //Two newInstance, One receives selectedExercises list.
    public static CreateWorkout newInstance(String name, List<Exercise> exercises) {
        CreateWorkout fragment = new CreateWorkout();
        Bundle args = new Bundle();
        args.putString("wName", name);
        args.putSerializable("list", (Serializable) exercises);
        fragment.setArguments(args);
        return fragment;
    }

    public static CreateWorkout newInstance(String name, List<Exercise> exercises, List<Exercise> selectedExercises) {
        CreateWorkout fragment = new CreateWorkout();
        Bundle args = new Bundle();
        args.putString("wName", name);
        args.putSerializable("list", (Serializable) exercises);
        args.putSerializable("selected", (Serializable) selectedExercises);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Check if the arguments is not null.
        if (getArguments() != null) {
            //Set up the name and exercise list.
            workoutName = getArguments().getString("wName");
            exerciseList = (List<Exercise>) getArguments().getSerializable("list");
            //If the arguments contains selected
            //Get the selected from the args
            //otherwise init selectedExercises with an empty ArrayList.
            if (getArguments().containsKey("selected")) {
                selectedExercises = (List<Exercise>) getArguments().getSerializable("selected");
            } else {
                selectedExercises = new ArrayList<>();
            }
        }
        //Name list is always empty Map.
        nameList = new HashMap<>();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_create_workout, container, false);
        setUpOptionsList(v); //Set up the optionsList.
        setUpButton(v); //Set up the button

        return v;
    }

    private void setUpButton(View v) {
        //Get the button and set up the click listener.
        Button add = v.findViewById(R.id.addButton);
        add.setOnClickListener(view -> AddWorkoutToPlan());
    }

    private void AddWorkoutToPlan() {
        //with the listener, call addWorkoutToPlan.
        Log.i("exercies", selectedExercises.toString());
        addExerciseListener.addWorkoutToPlan(workoutName, selectedExercises);
    }


    private void setUpOptionsList(View v) {
        //get the listView
        ListView listView = v.findViewById(R.id.optionList);
        //For every Exercise add the name to exercisesName and the whole exercise object to nameList.
        ArrayList<String> exerciseNames = new ArrayList<>();
        for (Exercise document : exerciseList) {
            String name = document.getName();
            if (name != null) {
                exerciseNames.add(name);
                nameList.put(name, document);
            }
        }
        //Creates the customStringAdapter.
        CustomStringAdapter adapter = new CustomStringAdapter(requireContext(),
                    android.R.layout.simple_list_item_multiple_choice, exerciseNames, selectedExercises, nameList);
        //Set the mode to Multiple Choice.
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        //Set the adapter
        listView.setAdapter(adapter);
        //set up search functionality
        setUpSearchFunction(v, listView);

    }
    //https://www.geeksforgeeks.org/searchview-in-android-with-listview/
    private void setUpSearchFunction(View v, ListView listView) {
        //Set up the searchView.
        SearchView searchView = v.findViewById(R.id.searchView);
        //get the adapter.
        ArrayAdapter<String> adapter = (ArrayAdapter<String>) listView.getAdapter();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //When the queryText is changed filter the adapter with newText.
                adapter.getFilter().filter(newText);
                return true;
            }
        });
    }




}