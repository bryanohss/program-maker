package com.example.programmaker.programmanager;

import android.content.Context;
import android.os.Bundle;

import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.programmaker.R;
import com.example.programmaker.adapters.ExerciseAdapter;
import com.example.programmaker.customC.WorkoutPlan;
    //Here you can edit your Workout
public class EditWorkout extends Fragment {

    //Listener.
    private EditWorkoutListener editWorkoutListener;
    private WorkoutPlan workoutPlan;
    private String workoutName;

    //Interface Listener
    public interface EditWorkoutListener {
        void addAnotherWorkout(String workoutName);

        void saveWorkoutPlan();
        void setUpButtons(View v, String workoutName);
    }

    public EditWorkout() {
        // Required empty public constructor
    }
    //Parent activity must implement the interface
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof EditWorkoutListener) {
            editWorkoutListener = (EditWorkoutListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement EditWorkoutListener");
        }
    }

    public static EditWorkout newInstance(String name, WorkoutPlan program) {
        EditWorkout fragment = new EditWorkout();
        Bundle args = new Bundle();
        //The name and the of the workout and the program.
        args.putString("name", name);
        args.putSerializable("program", program);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //Init the variables
            workoutName = getArguments().getString("name");
            workoutPlan = (WorkoutPlan) getArguments().getSerializable("program");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_edit_workout, container, false);
        //Get the nestedScrollView
        NestedScrollView scrollView = v.findViewById(R.id.nested_scroll);
        //Get the recyclerView
        RecyclerView lst = v.findViewById(R.id.exercie_view);
        //Set the layoutManager
        lst.setLayoutManager(new LinearLayoutManager(requireContext()));
        //Create the custom adapter and set the adapter
        ExerciseAdapter adapter = new ExerciseAdapter(requireContext(), workoutPlan.getWorkout(workoutName));
        lst.setAdapter(adapter);

        //Call set up button on the listener.
        editWorkoutListener.setUpButtons(v, workoutName);
        return v;
    }

}
