package com.example.programmaker.programmanager;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.example.programmaker.R;
import com.example.programmaker.createprogram.CreateWorkout;
import com.example.programmaker.customC.Exercise;
import com.example.programmaker.customC.LanguageManager;
import com.example.programmaker.customC.RecipeHolder;
import com.example.programmaker.customC.WorkoutPlan;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FieldValue;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
//Program manager
//Responsible for edit, save log and more for the program.
public class ProgramManager extends AppCompatActivity implements EditWorkout.EditWorkoutListener, CreateWorkout.AddExerciseListener {

    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private FirebaseUser currentUser;
    private WorkoutPlan program;
    private HashMap<String, Object> logData;
    private LanguageManager languageManager;
    private List<Exercise> exercises;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_program_manager);

        //Get the intent
        //Load the program, lodData and bool showButtons.
        Intent in = getIntent();
        program = new WorkoutPlan((HashMap<String, Object>) in.getSerializableExtra("program"));
        logData = (HashMap<String, Object>) in.getSerializableExtra("logData");
        boolean showButtons = in.getBooleanExtra("buttons", false); //Default false
        assert logData != null;

        //Create the showProgram Fragment and show it.
        ShowProgramFragment showProgramFragment = ShowProgramFragment.newInstance(program, showButtons);
        getSupportFragmentManager().beginTransaction().add(R.id.program_frame, showProgramFragment).commit();

    }
    //Start workout is called when a user clicks on start porgram
    public void startWorkout(String workoutName) {
        //create new recipe holder
        //The only use is to send over data.
        RecipeHolder temp = new RecipeHolder(program.getWorkout(workoutName), "");
        ShowWorkoutFragment showWorkoutFragment = ShowWorkoutFragment.newInstance(workoutName, temp);
        getSupportFragmentManager().beginTransaction().replace(R.id.program_frame, showWorkoutFragment).commit();

    }

    public void SaveOnLog(List<Exercise> exerciseList, String workoutName) {
        //Get current Data
        Date currentDate = Calendar.getInstance().getTime();
        //Init finalLog and exerciseLog.
        Map<String, Object> finalLog = new HashMap<>();
        Map<String, Object> exerciseLog = new HashMap<>();
        //For every exercise
        //Create a tmp hashmap, get the info of the exercise (Ex. [{reps:, sets:, weight:}]
        //put into ExerciseLog.
        for (Exercise i : exerciseList) {
            Map<String, Object> tmp = new HashMap<>();
            tmp.put("info", i.getInfo());
            exerciseLog.put(i.getName(), tmp);
        }
        //Put exerciseLog into finalLog
        //Put the date into finalLog
        //Put the name of the workout. (Ex. Day 1)
        finalLog.put("exercies", exerciseLog);
        finalLog.put("date", currentDate.toString());
        finalLog.put("workout", workoutName);

        //Get documentReference
        DocumentReference userRef = db.collection("users").document(currentUser.getUid());
        //Update workoutLog. + program.getName(). (nested) Append into existing array.
        userRef.update("workoutLog." + program.getName(), FieldValue.arrayUnion(finalLog))
                .addOnSuccessListener(unused -> {
                    //Return feedback.
                    Toast.makeText(ProgramManager.this, "Log Saved!", Toast.LENGTH_SHORT).show();
                    onBackPressed();
                });
    }

    //Before on Create.
    //Get called when the base Context in being attached.
    @Override
    protected void attachBaseContext(Context newBase) {
        languageManager = new LanguageManager(newBase); //Init languageManager with the context.
        super.attachBaseContext(languageManager.setLocale(newBase)); //LanguaManager setLocale returns a context with the configuration of current Locale
    }

    @Override
    public void onStart() {
        super.onStart();
        //On Start init variables
        //Load savedPreferences
        //Get Exercises.
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        currentUser = mAuth.getCurrentUser();
        languageManager.loadSavedLocalePreferences(this);
        getExercises();
    }


    public void viewLog(String program) {
        //Create ShowProgramLog Fragment with chosen program and the logData
        ShowProgramLog showProgramLog = ShowProgramLog.newInstance(logData, program);
        //Replace the fragment
        getSupportFragmentManager().beginTransaction().replace(R.id.program_frame, showProgramLog).commit();

    }

    public void shareWorkout(String name) {
        //Inflate the dialogView with custom Share Layout File.
        View dialogView = LayoutInflater.from(this).inflate(R.layout.share_layout, null);
        //Create AlertDialog builder, set view and title
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(dialogView);
        builder.setTitle(R.string.share_text);
        //Generate the code to import workout.
        String code = generateCode(name);
        //Get the TextView for the Code and set it.
        TextView codeView = dialogView.findViewById(R.id.code_text);
        codeView.setText(code);
        //Set up the copy Button.
        Button copyBtn = dialogView.findViewById(R.id.copy);
        AlertDialog alertDialog = builder.create();
        copyBtn.setOnClickListener(view -> copyToClipBoard(code, alertDialog));
        //Show
        alertDialog.show();
    }

    private void copyToClipBoard(String code, AlertDialog alertDialog) {
        //Init Clipboard manager.
        ClipboardManager clipboardManager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
        //Set the clipData.
        ClipData clipData = ClipData.newPlainText("Copied Text", code);
        //Set primary
        clipboardManager.setPrimaryClip(clipData);
        //Show feedback
        Toast.makeText(this, R.string.copy_done, Toast.LENGTH_SHORT).show();
        alertDialog.dismiss();
    }

    //The code is the user's UID and the name of the workout.
    private String generateCode(String name) {
        return currentUser.getUid() + "-" + name;
    }

    //Init editProgram fragment and show it.
    //this is to edit the program.
    public void editProgram() {
        EditProgramFragment editProgramFragment = EditProgramFragment.newInstance(program);
        getSupportFragmentManager().beginTransaction().replace(R.id.program_frame, editProgramFragment).commit();

    }
    @Override
    public void setUpButtons(View v, String workoutName) {
        //Get the buttons and set the clickListeners.
        Button savePlan = v.findViewById(R.id.save_workout);
        savePlan.setText(R.string.update_workout);
        savePlan.setOnClickListener(view -> saveWorkoutPlan());

        Button anotherButton = v.findViewById(R.id.add_another);
        anotherButton.setText(R.string.add_exercies);
        anotherButton.setOnClickListener(view -> addAnotherWorkout(workoutName));
    }

    @Override
    public void addAnotherWorkout(String workoutName) {
        //Get the existing exercises for current program
        //CreateWorkout Fragment is to add more workouts to current program.
        List<Exercise> selected = program.getWorkout(workoutName);
        CreateWorkout createWorkout = CreateWorkout.newInstance(workoutName, exercises, selected);
        getSupportFragmentManager().beginTransaction().replace(R.id.program_frame, createWorkout).commit();
    }



    @Override
    public void saveWorkoutPlan() {
        //Get the current DocumentReference
        DocumentReference userRef = db.collection("users").document(Objects.requireNonNull(mAuth.getCurrentUser()).getUid());
        userRef.get().addOnCompleteListener(task -> {
            //get everything.
            if (task.isSuccessful()) {
                //Get the documentSnapshot
                DocumentSnapshot doc = task.getResult();
                //Get the list of myWorkouts
                List<HashMap<String, Object>> list = (List<HashMap<String, Object>>) doc.getData().get("myWorkouts");
                //Get the name of current program
                String programName = program.getName();
                //We must find the index of the current program to replace it with the updated one.
                int indexToChange = -1;
                for (int i = 0; i < list.size(); i++) {
                    WorkoutPlan tmp = new WorkoutPlan(list.get(i));
                    if (programName.equals(Objects.requireNonNull(tmp.getName()))) {
                        indexToChange = i;
                        break;
                    }
                }

                //If we get a match.
                if (indexToChange != -1) {
                    //program to Map
                    HashMap<String, Object> updatedProgram = program.toMap();
                    list.set(indexToChange, updatedProgram); // Update the HashMap in the list
                    Map<String, Object> updatedData = new HashMap<>(); //The updated Data
                    //Update the whole list.
                    updatedData.put("myWorkouts", list);
                    userRef.update(updatedData).addOnSuccessListener(unused -> {
                        //return feedback.
                        Toast.makeText(ProgramManager.this, "Updated!", Toast.LENGTH_SHORT).show();
                        onBackPressed();
                    });
                }
            }
        });
    }



    @Override
    public void addWorkoutToPlan(String workoutName, List<Exercise> selectedExercises) {
        //Get the list of existing exercises
        List<Exercise> workoutExercises = program.getWorkout(workoutName);
        //For every selected exercise
        //if the program doesn't contain the exercise in selected, add them.
        for (Exercise selectedExercise : selectedExercises) {
            if (!workoutExercises.contains(selectedExercise)) {
                program.addExercise(workoutName, selectedExercise);
            }
        }
        //For every exercise in current program exercises
        //if selected doesn't contain them, delete them
        for (Exercise workoutExercise : workoutExercises) {
            if (!selectedExercises.contains(workoutExercise)) {
                program.removeExercise(workoutName, workoutExercise.getName());
            }
        }
        startEdit(workoutName);
    }

    //StartEdit is for editing the workout
    public void startEdit(String workoutName) {
        EditWorkout editWorkout = EditWorkout.newInstance(workoutName, program);
        getSupportFragmentManager().beginTransaction().replace(R.id.program_frame, editWorkout).commit();

    }

    private void getExercises() {
        //Get the CollectionReference of exercises
        CollectionReference collectionReference = db.collection("exercises");
        //Get all
        collectionReference.get()
                .addOnSuccessListener(queryDocumentSnapshots -> {
                    List<Exercise> result = new ArrayList<>();
                    //for every exercise add them to exercises.
                    for (QueryDocumentSnapshot doc : queryDocumentSnapshots) {
                        Exercise tmp = doc.toObject(Exercise.class);
                        result.add(tmp);
                    }
                    exercises = result;
                });
    }


    public void addToMyPrograms(WorkoutPlan program) {
        //Get the documentReference
        DocumentReference userRef = db.collection("users").document(currentUser.getUid());
        //we want to append our program to the field of "myWorkouts".
        userRef.update("myWorkouts", FieldValue.arrayUnion(program))
                //On success, return feed back and createLog field.
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void unused) {
                        Toast.makeText(ProgramManager.this, "Program Saved!",
                                Toast.LENGTH_SHORT).show();
                        createLog();
                        onBackPressed();
                    }

                    private void createLog() {
                        //Create empty List.
                        //Get the name of the exercise
                        List<Object> empty = new ArrayList<>();
                        String workoutPlanName = program.getName();
                        //In a new Map use Nested fields to update the workoutPlanName in workoutLog field in Firestore.
                        Map<String, Object> updateData = new HashMap<>();
                        updateData.put("workoutLog." + workoutPlanName, empty);
                        //Update the data.
                        userRef.update(updateData);

                    }
                    //On Failure return feedback.
                }).addOnFailureListener(e -> Toast.makeText(ProgramManager.this, "Something is wrong, please try later",
                        Toast.LENGTH_SHORT).show());

    }

}