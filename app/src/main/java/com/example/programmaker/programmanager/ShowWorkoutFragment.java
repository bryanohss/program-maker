package com.example.programmaker.programmanager;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.programmaker.R;
import com.example.programmaker.adapters.ExerciseAdapter;
import com.example.programmaker.customC.Exercise;
import com.example.programmaker.customC.RecipeHolder;

import java.util.List;

//This shows given workout
public class ShowWorkoutFragment extends Fragment {
    private String workoutName;
    private List<Exercise> exerciseList;
    
    public ShowWorkoutFragment() {
        // Required empty public constructor
    }
    public static ShowWorkoutFragment newInstance(String workoutName, RecipeHolder workouts) {
        ShowWorkoutFragment fragment = new ShowWorkoutFragment();
        Bundle args = new Bundle();
        args.putSerializable("list", workouts);
        args.putString("workoutName", workoutName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            workoutName = getArguments().getString("workoutName");
            RecipeHolder tmp = (RecipeHolder) getArguments().getSerializable("list");
            assert tmp != null;
            exerciseList = tmp.getWorkout();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_show_workout, container, false);
        //Set the title
        TextView title = v.findViewById(R.id.title_w);
        title.setText(workoutName);
        //Set the list and saveLog Button.
        setUpList(v);
        Button saveLog = v.findViewById(R.id.save_log);
        saveLog.setOnClickListener(view -> saveWorkoutLog());
        return v;
    }
    //Call method on ProgramManager
    private void saveWorkoutLog() {
        ((ProgramManager)requireActivity()).SaveOnLog(exerciseList, workoutName);
    }

    private void setUpList(View v) {
        //Create Adapter and set to the lst.
        Log.i("List", exerciseList.toString());
        ExerciseAdapter adapter = new ExerciseAdapter(requireContext(), exerciseList);
        RecyclerView lst = v.findViewById(R.id.workout_lst);
        lst.setLayoutManager(new LinearLayoutManager(requireContext()));
        lst.setAdapter(adapter);
    }

}