package com.example.programmaker.programmanager;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.programmaker.R;
import com.example.programmaker.adapters.LogAdapter;

import java.util.HashMap;
import java.util.List;
//This shows the log of given program Name
public class ShowProgramLog extends Fragment {

    private HashMap<String, Object> logData;
    private String programName;

    public ShowProgramLog() {
        // Required empty public constructor
    }


    public static ShowProgramLog newInstance(HashMap<String, Object> logData, String program)  {
        ShowProgramLog fragment = new ShowProgramLog();
        Bundle args = new Bundle();
        //Put logData and programName in args.
        args.putSerializable("logData", logData);
        args.putString("program", program);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //Get the data.
            logData = (HashMap<String, Object>) getArguments().getSerializable("logData");
            programName = getArguments().getString("program");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_show_program_log, container, false);
        //Set up the tile of the programName
        TextView title = v.findViewById(R.id.program_title);
        title.setText(programName);
        //Get the RecyclerView
        RecyclerView lst = v.findViewById(R.id.log_lst);
        //Create Custom Adapter
        LogAdapter logAdapter = new LogAdapter(requireContext(), (List<Object>) logData.get(programName));
        //Set LinearLayout Manager.
        LinearLayoutManager layoutManager = new LinearLayoutManager(requireContext());
        //Set the manager and the adapter
        lst.setLayoutManager(layoutManager);
        lst.setAdapter(logAdapter);
        return v;
    }
}