package com.example.programmaker.programmanager;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.programmaker.R;
import com.example.programmaker.adapters.WorkoutAdapter;
import com.example.programmaker.customC.Exercise;
import com.example.programmaker.customC.WorkoutPlan;

import java.util.HashMap;
import java.util.List;
//In this fragment can the user edit how much sets, reps and weight they gonna have.
public class EditProgramFragment extends Fragment implements WorkoutAdapter.WorkoutAdapterListener {
    private WorkoutPlan program;
    public EditProgramFragment() {
        // Required empty public constructor
    }
    public static EditProgramFragment newInstance(WorkoutPlan program) {
        EditProgramFragment fragment = new EditProgramFragment();
        Bundle args = new Bundle();
        //Put program in args.
        args.putSerializable("program", program);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //get the program from args
            program = (WorkoutPlan) getArguments().getSerializable("program");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_edit_program, container, false);
        //get the recyclerView.
        RecyclerView recyclerView = v.findViewById(R.id.recycler_lst);
        //Set the layoutManager
        recyclerView.setLayoutManager(new LinearLayoutManager(requireContext()));
        //Create the workoutAdapter with required parameters.
        WorkoutAdapter adapter = new WorkoutAdapter(requireContext(), (HashMap<String, List<Exercise>>) program.getWorkouts(), this, true);
        //Set the adapter.
        recyclerView.setAdapter(adapter);
        return v;
    }


    @Override
    public void onWorkoutStartClicked(String workoutName) {
        //Implements the interface of the adapter
        //Call programManager startEdit.
        ((ProgramManager)requireActivity()).startEdit(workoutName);
    }
}