package com.example.programmaker.programmanager;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.programmaker.R;
import com.example.programmaker.adapters.WorkoutAdapter;
import com.example.programmaker.customC.Exercise;
import com.example.programmaker.customC.WorkoutPlan;

import java.util.HashMap;
import java.util.List;

//This fragment shows the information of the chosen workoutPlan.
public class ShowProgramFragment extends Fragment implements WorkoutAdapter.WorkoutAdapterListener{

    private WorkoutPlan program;
    private boolean showButtons;

    public ShowProgramFragment() {
    }

    public static ShowProgramFragment newInstance(WorkoutPlan program, boolean showButtons) {
        ShowProgramFragment fragment = new ShowProgramFragment();
        Bundle args = new Bundle();
        //Put the program and the bool into the args
        args.putSerializable("program", program);
        args.putBoolean("show", showButtons);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            //Init program with the args.
            program = (WorkoutPlan) getArguments().getSerializable("program");
            showButtons =  getArguments().getBoolean("show");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_show_program, container, false);
        TextView title = v.findViewById(R.id.program_name);
        //Check if program isn't null set up the title and set up the workoutList.
        if(program != null){
            title.setText(program.getName());
            setUpWorkoutList(v);
        }
        //Set up buttons.
        setUpButtons(v);

        return v;
    }

    private void setUpButtons(View v) {
        //Set buttons and set their clickListener.
        Button logBtn = v.findViewById(R.id.view_log_btn);
        logBtn.setOnClickListener(view -> viewLog());

        Button shareBtn = v.findViewById(R.id.share_btn);
        shareBtn.setOnClickListener(view -> shareWorkout());

        Button editBtn = v.findViewById(R.id.edit_btn);
        editBtn.setOnClickListener(view -> editProgram());

        Button saveToMyWorkouts = v.findViewById(R.id.add_to_me);
        saveToMyWorkouts.setOnClickListener(view -> addToMyPrograms(program));

        //If showButtons is false
        //Show the log button, share button and the edit button.
        //Else show only the saveToMyWorkouts Button.
        //This because we don't want to share or edit if we haven't saved them.
        if (!showButtons) {
            saveToMyWorkouts.setVisibility(View.GONE);
            logBtn.setVisibility(View.VISIBLE);
            shareBtn.setVisibility(View.VISIBLE);
            editBtn.setVisibility(View.VISIBLE);
        } else {
            saveToMyWorkouts.setVisibility(View.VISIBLE);
            logBtn.setVisibility(View.GONE);
            shareBtn.setVisibility(View.GONE);
            editBtn.setVisibility(View.GONE);
        }
    }

    //Call method on ProgramManager
    private void addToMyPrograms(WorkoutPlan program) {
        ((ProgramManager)requireActivity()).addToMyPrograms(program);
    }

    //Call method on ProgramManager
    private void editProgram() {
        ((ProgramManager)requireActivity()).editProgram();
    }
    //Call method on ProgramManager
    private void shareWorkout() {
        ((ProgramManager)requireActivity()).shareWorkout(program.getName());
    }
    //Call method on ProgramManager
    private void viewLog() {
        ((ProgramManager)requireActivity()).viewLog(program.getName());
    }


    private void setUpWorkoutList(View v) {
        //Get the recyclerView
        //Create custom Adapter with the workoutList.
        RecyclerView lst = v.findViewById(R.id.workout_list);
        lst.setLayoutManager(new LinearLayoutManager(requireContext()));
        WorkoutAdapter adapter = new WorkoutAdapter(requireContext(), (HashMap<String, List<Exercise>>) program.getWorkouts(), this,false);
        lst.setAdapter(adapter);
    }
    //Call method on ProgramManager
    @Override
    public void onWorkoutStartClicked(String workoutName) {
        ((ProgramManager)requireActivity()).startWorkout(workoutName);

    }
}