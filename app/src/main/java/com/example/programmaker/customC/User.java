package com.example.programmaker.customC;

import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
//Custom class that represents a user.
@IgnoreExtraProperties
public class User {
    private String username;
    private String email;
    private List<Object> myWorkouts;
    private HashMap<String, Object> workoutLog;

    //Empty required for FireStore.
    public User() {
        myWorkouts = new ArrayList<>();
        workoutLog = new HashMap<>();
    }
    //Used constructor. init all variables.
    public User(String username, String email) {
        this.username = username;
        this.email = email;
        myWorkouts = new ArrayList<>();
        workoutLog = new HashMap<>();
    }

    //getters and setters.
    public List<Object> getMyWorkouts() {
        return myWorkouts;
    }

    public void setMyWorkouts(List<Object> myWorkouts) {
        this.myWorkouts = myWorkouts;
    }

    public HashMap<String, Object> getWorkoutLog() {
        return workoutLog;
    }

    public void setWorkoutLog(HashMap<String, Object> workoutLog) {
        this.workoutLog = workoutLog;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
