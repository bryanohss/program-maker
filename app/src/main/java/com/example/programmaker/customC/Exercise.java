package com.example.programmaker.customC;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
//Custom class Exercise
//Implemets Serializable to be abled to send it in args.

public class Exercise implements Serializable {

    private String name;
    private String desc;
    private List<HashMap<String, Object>> info;

    public Exercise() {
        this.info = new ArrayList<>();
        addColumnToInfo();
    }

    //Map to exercise.
    public Exercise(Map<String, Object> map) {
        this.name = Objects.requireNonNull(map.get("name")).toString();
        this.desc = Objects.requireNonNull(map.get("desc")).toString();
        this.info = (List<HashMap<String, Object>>) map.get("info");
    }

    //Constructor, name and description is used.
    public Exercise(String name, String desc) {
        this.name = name;
        this.desc = desc;
        this.info = new ArrayList<>();
        addColumnToInfo(); //Add column
    }

    public void addColumnToInfo(){
        //Create a new row of information.
        HashMap<String, Object> tmp = new HashMap<>();
        tmp.put("sets", 0);
        tmp.put("reps", 0);
        tmp.put("weight", 0);
        info.add(tmp);
    }

    //Getter and setters for FireStore.
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public List<HashMap<String, Object>> getInfo() {
        return info;
    }

    public void setInfo(List<HashMap<String, Object>> info) {
        this.info = info;
    }

    //Class to MAP.
    public Map<String, Object> toMap() {
        //Create the map and set the name and desc.
        HashMap<String, Object> map = new HashMap<>();
        map.put("name", name);
        map.put("desc", desc);

        // Convert the info list to a format that Firestore can handle
        List<Map<String, Object>> formattedInfo = new ArrayList<>();
        //For every item in desc.
        for (HashMap<String, Object> infoItem : info) {
            //New Map.
            //Set the key and value for sets,reps and weight.
            Map<String, Object> formattedInfoItem = new HashMap<>();
            formattedInfoItem.put("sets", infoItem.get("sets"));
            formattedInfoItem.put("reps", infoItem.get("reps"));
            formattedInfoItem.put("weight", infoItem.get("weight"));
            //and them to the List.
            formattedInfo.add(formattedInfoItem);
        }
        //Put them into the map.
        map.put("info", formattedInfo);
        //return it.
        return map;
    }
}
