package com.example.programmaker.customC;

import com.example.programmaker.customC.Exercise;
import com.google.firebase.firestore.IgnoreExtraProperties;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
//Represents a workoutPlan
@IgnoreExtraProperties
public class WorkoutPlan implements Serializable {
    private String name;
    private Map<String, List<Exercise>> workouts;
    private Long focus;

    public WorkoutPlan() {
        workouts = new HashMap<>();
        name = "";
        focus = 0L; // 0 LONG used for importing existing plans from fireStore. 0
    }
    //Map to workout.
    public WorkoutPlan(HashMap<String, Object> map) {
        //Init workouts as empty.
        workouts = new HashMap<>();

        if (map != null) {
            //get the name and focus.
            this.name = Objects.requireNonNull(map.get("name")).toString();
            this.focus = (Long) map.get("focus");


            Map<String, List<Map<String, Object>>> rawData = (Map<String, List<Map<String, Object>>>) map.get("workouts");
            // Iterate through the rawData https://stackoverflow.com/questions/1066589/iterate-through-a-hashmap
            //For every entry in rawData
            for (Map.Entry<String, List<Map<String, Object>>> entry : rawData.entrySet()) {
                String workoutName = entry.getKey(); //the key is the name
                List<Map<String, Object>> lstOfExercises = entry.getValue(); //List of exercises.
                //Empty list of exercises.
                List<Exercise> exercises = new ArrayList<>();

                // Convert every Map<String, Object> to Exercise Object
                for (Map<String, Object> exerciseMap : lstOfExercises) {
                    Exercise exercise = new Exercise(exerciseMap);
                    exercises.add(exercise);
                }

                workouts.put(workoutName, exercises);
            }

        }
    }

    //Standard constructor.
    public WorkoutPlan(String name, Map<String, List<Exercise>> workouts) {
        this.name = name;
        this.workouts = workouts;
    }
    //Setters and getters for FireStore.
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, List<Exercise>> getWorkouts() {
        return workouts;
    }

    public void setWorkouts(Map<String, List<Exercise>> workouts) {
        this.workouts = workouts;
    }
    public void setFocus(Long i) {
        this.focus = i;
    }

    public Long getFocus() {
        return this.focus;
    }

    public void addWorkout(String workoutName, List<Exercise> selectedExersices) {
        workouts.put(workoutName, selectedExersices);
    }

    public List<Exercise> getWorkout(String workoutName) {
        return (List<Exercise>) workouts.get(workoutName);
    }

    //Object to Map.
    public HashMap<String, Object> toMap() {
        //create the map
        HashMap<String, Object> map = new HashMap<>();
        //Put the name
        map.put("name", name);
        //Create formatted workouts
        Map<String, List<Map<String, Object>>> formattedWorkouts = new HashMap<>();
        //For every workout.
        for (Map.Entry<String, List<Exercise>> entry : workouts.entrySet()) {
            String workoutName = entry.getKey(); //Name of the workout
            List<Exercise> exercises = entry.getValue(); //the list of exercises.

            List<Map<String, Object>> formattedExercises = new ArrayList<>(); //Formated exercises
            for (Exercise exercise : exercises) {
                formattedExercises.add(exercise.toMap()); //add every exercise after calling toMap.
            }
            formattedWorkouts.put(workoutName, formattedExercises); //Put every workout to the Map.
        }
        map.put("workouts", formattedWorkouts); //put the whole packed into the map under the key workouts.

        return map; //return map
    }
    
    public void removeExercise(String workoutName, String tmpString){
        List<Exercise> workoutList = workouts.get(workoutName); //Get the list of exercises from correct workout
        assert workoutList != null;
        for (Exercise tmp : workoutList){
            if(tmpString.equals(tmp.getName())){ //tmpString is the name of the chosen exercise to delete
                workoutList.remove(tmp); //If equal remove it.
                break;
            }
        }
    }

    public void addExercise(String workoutName, Exercise tmp) {
        List<Exercise> list = workouts.get(workoutName); //get the correct list of exercises
        assert list != null;
        list.add(tmp); //add the exercise.
    }
}
