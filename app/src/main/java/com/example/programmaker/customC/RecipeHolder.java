package com.example.programmaker.customC;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

public class RecipeHolder implements Serializable {
    List<HashMap<String, Object>> recipeList;
    HashMap<String, Object> meal;
    List<Exercise> workout;

    public RecipeHolder(List<Exercise> workouts, String i) {
        this.workout = workouts;
    }

    public RecipeHolder(List<HashMap<String, Object>> list) {
        this.recipeList = list;
    }

    public RecipeHolder(HashMap<String, Object> meal) {
        this.meal = meal;
    }

    public List<HashMap<String, Object>> getRecipeList() {
        return recipeList;
    }

    public HashMap<String, Object> getMeal() {
        return meal;
    }

    public List<Exercise> getWorkout() {
        return workout;
    }
}
