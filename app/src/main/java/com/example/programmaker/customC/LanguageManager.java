package com.example.programmaker.customC;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.LocaleList;
import android.util.Log;

import java.util.Locale;
//Language manager is responsible for the changing of the language of the app.
//Also restoring the current Locale from SharedPreferences.
public class LanguageManager {
    private static final String PREF_NAME = "MyPrefs";
    private static final String KEY_CURRENT_LOCALE = "currentLocale";
    private Locale currentLocale;
    //Shared Preferences
    //https://developer.android.com/training/data-storage/shared-preferences
    //Locale
    //https://developer.android.com/reference/java/util/Locale'
    //Configuration
    //https://developer.android.com/reference/android/content/res/Configuration

    //Constructor takes in a context.
    public LanguageManager(Context context) {
        //get the sharedPreferences in private mode.
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        //get the saved Locale, default value in none = null
        String savedLocale = sharedPreferences.getString(KEY_CURRENT_LOCALE, null);

        //Init currentLocale with the savedOne else just get the default.
        if (savedLocale != null) {
            currentLocale = new Locale(savedLocale);
        } else {
            currentLocale = Locale.getDefault();
        }
    }

    //Return the context of updateResources.
    public Context setLocale(Context context) {
        return updateResources(context, currentLocale);
    }

    //Changes the language
    public void changeLanguage(Context context, String language) {
        //Create a newLocale with the chosenOne.
        Locale newLocale = new Locale(language);
        //Update the currentLocale
        currentLocale = newLocale;
        //Call update resources with the given context and newLocale.
        updateResources(context, newLocale);
        //Save the CurrentLocale to sharedPreferences.
        saveCurrentLocaleToPrefs(context, language);
    }

    private Context updateResources(Context context, Locale newLocale) {
        //Get the configuration from the contexts.
        Configuration configuration = new Configuration(context.getResources().getConfiguration());
        //Set the locale with the new one
        configuration.setLocale(newLocale);
        //Set the default with the new one
        Locale.setDefault(newLocale);
        //rewrite the context with the new configurations.
        context = context.createConfigurationContext(configuration);
        //return it.
        return context;
    }

    private void saveCurrentLocaleToPrefs(Context context, String language) {
        //Get the sharedPreferences with the context.
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        //Init the editor.
        SharedPreferences.Editor editor = sharedPreferences.edit();
        //Put the language string into the key.
        editor.putString(KEY_CURRENT_LOCALE, language);
        //aply changes.
        editor.apply();
    }


    public void loadSavedLocalePreferences(Context context) {
        //get the sharedPreferences with private mode.
        SharedPreferences sharedPreferences = context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
        //Get the saved.
        String savedLocale = sharedPreferences.getString(KEY_CURRENT_LOCALE, null);
        //if there is a savedLocale update the resources.
        if (savedLocale != null) {
            Locale newLocale = new Locale(savedLocale);
            updateResources(context, newLocale);
        }
    }
}
