package com.example.programmaker.auth;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.os.LocaleList;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.programmaker.customC.LanguageManager;
import com.example.programmaker.main.NavMain;
import com.example.programmaker.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;
//Main activity, Login
public class MainActivity extends AppCompatActivity {
    private FirebaseAuth mAuth; //Firebase Auth
    private LanguageManager languageManager; //Custom Class languageManager

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Set upp languageSpinner
        setUpLanguageSpinner();
        //Set up these buttons
        Button loginBtn = findViewById(R.id.loginBtn);
        Button registerBtn = findViewById(R.id.registerBtn);

        //Set their OnClickListeners.
        registerBtn.setOnClickListener(view -> registerActivity());
        loginBtn.setOnClickListener(view -> loginUser());

    }
    private void registerActivity() {
        //Start the register Activity.
        Intent in = new Intent(this, RegisterActivity.class);
        Log.i("Default in Main", Locale.getDefault().toString());
        startActivity(in); //start the activity
    }

    private void loginUser() {
        //Get the editTexts
        EditText email = findViewById(R.id.EmailText);
        EditText password = findViewById(R.id.PasswordText);
        if(!email.getText().toString().equals("") && !password.getText().toString().equals("")){
            //Use the built in method to login the user.
            mAuth.signInWithEmailAndPassword(email.getText().toString(), password.getText().toString())
                    .addOnCompleteListener(this, task -> {
                        if (task.isSuccessful()) {
                            //If Successful continue to the main APP
                            continueMainApp();
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(MainActivity.this, "Incorrect email or password",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
        }else{
            Toast.makeText(MainActivity.this, "Please fill the text inputs.",
                    Toast.LENGTH_SHORT).show();
        }
    }
    private void setUpLanguageSpinner() {
        //Configure the spinner.
        Spinner spinner = findViewById(R.id.spinner);
        //Create the adapter with custom Array.
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.language_options, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter); //set the adapter.

        //Get the current locale.
        String currentLocaleString = Locale.getDefault().toString();
        int selectedIndex;
        //Only two options. "sv" and "eng".
        if (currentLocaleString.equals("sv")) {
            selectedIndex = 1;
        } else {
            selectedIndex = 0;
        }
        //Set the current locale to selected.
        spinner.setSelection(selectedIndex);
        //Set the on item Selected.
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int pos, long l) {
                String selection = adapterView.getItemAtPosition(pos).toString();
                //create sets for possible "selection".
                Set<String> se = new HashSet<>(Arrays.asList("Swedish", "Svenska"));
                Set<String> eng = new HashSet<>(Arrays.asList("English", "Engelska"));

                // Check if the selected language is in the set of supported languages
                if (se.contains(selection)) {
                    changeLan("sv");
                } else if (eng.contains(selection)) {
                    changeLan("en");
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }
    private void continueMainApp() {
        //Create the intent.
        Intent in = new Intent(this, NavMain.class);
        Log.i("Default in Main", Locale.getDefault().toString());
        startActivity(in); //Start the activity.
    }


    private void changeLan(String lan) {
        //Get the currentConfig.
        Configuration currentConfig = getResources().getConfiguration();
        LocaleList currentLocales = currentConfig.getLocales(); //Get the current locales.
        String currentLanguage = currentLocales.get(0).getLanguage();
        // Check if the selected language is different from the current language
        //If so change the language.
        if (!lan.equals(currentLanguage)) {
            languageManager.changeLanguage(this, lan); //Call changeLanguage.
            recreate(); //reCreate the activity.
        }
    }
    //Before on Create.
    //Get called when the base Context in being attached.
    @Override
    protected void attachBaseContext(Context newBase) {
        languageManager = new LanguageManager(newBase); //Init languageManager with the context.
        super.attachBaseContext(languageManager.setLocale(newBase)); //LanguageManager setLocale returns a context with the configuration of current Locale
    }

    @Override
    public void onStart() {
        super.onStart();
        //Init mAuth
        mAuth = FirebaseAuth.getInstance();
        //On Start if there is current User
        //Redirect them to Main App.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            continueMainApp();
        }
        //If not. Load Saved Preferences.
        //This will load the locale in SharedPreferences.
        //Adhd update the resources if there is a saved Locale.
        languageManager.loadSavedLocalePreferences(this);
    }
}
