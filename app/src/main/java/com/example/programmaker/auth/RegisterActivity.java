package com.example.programmaker.auth;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.example.programmaker.customC.LanguageManager;
import com.example.programmaker.customC.User;
import com.example.programmaker.main.NavMain;
import com.example.programmaker.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.SetOptions;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;

import javax.annotation.Nullable;
//Register Activity.
public class RegisterActivity extends AppCompatActivity {
    private FirebaseAuth mAuth;
    private FirebaseFirestore db;
    private LanguageManager languageManager;
    private  GoogleSignInClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //Init the frames

        MainRegister mainFrame = MainRegister.newInstance();
        //Set up the google Options.
        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        client = GoogleSignIn.getClient(this, options); //Get the client.

        //Init mAuth and db.
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();
        //Display the first fragment.
        getSupportFragmentManager().beginTransaction().add(R.id.framePlaceholder, mainFrame).commit();

    }

    public void registerUser(String userText, String emailText, String passText) {
        final String username = userText;
        final String email = emailText;
        final String pass = passText;
        //Use the builtInMethod to create a user.
        if(!email.equals("") && !pass.equals("") && !userText.equals("")){
            mAuth.createUserWithEmailAndPassword(email, pass)
                    .addOnCompleteListener(this, task -> {
                        if (task.isSuccessful()) {
                            //If Successful
                            String userId = Objects.requireNonNull(mAuth.getCurrentUser()).getUid();
                            User user = new User(username, email); //Create new User

                            db.collection("users").document(userId).set(user); //Save the User in database.
                            Toast.makeText(RegisterActivity.this, "Authentication success.",
                                    Toast.LENGTH_SHORT).show();
                            //Call the nextFrame, next step.
                            nextFrame();
                        } else {
                            Log.w("TASK", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    });
        } else{
            Toast.makeText(RegisterActivity.this, "please fill the inputs.",
                    Toast.LENGTH_SHORT).show();
        }

    }

    public void nextFrame() {
        SecondaryRegister secondaryFrame = SecondaryRegister.newInstance();
        getSupportFragmentManager().beginTransaction().replace(R.id.framePlaceholder, secondaryFrame).commit();
    }
    //Before on Create.
    //Get called when the base Context in being attached.
    @Override
    protected void attachBaseContext(Context newBase) {
        languageManager = new LanguageManager(newBase); //Init languageManager with the context.
        super.attachBaseContext(languageManager.setLocale(newBase)); //LanguaManager setLocale returns a context with the configuration of current Locale
    }

    @Override
    public void onStart() {
        super.onStart();
        //On Start if there is current User
        //Redirect them to Main App.
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            continueMainApp();
        }
        //If not. Load Saved Preferences.
        //This will load the locale in SharedPreferences.
        //Adhd update the resources if there is a saved Locale.
        languageManager.loadSavedLocalePreferences(this);
    }

    public void updatePreferences(String gender, String dGoals, String tGoals) {
        //Update the preferences.
        FirebaseUser currentUser = mAuth.getCurrentUser(); //Get the current User.
        assert currentUser != null;
        String userId = currentUser.getUid(); //Get the uid.
        DocumentReference userRef = db.collection("users").document(userId); //Get the document reference

        //Create arrays with possible options.
        String[] genderArray = getResources().getStringArray(R.array.gender_options);
        String[] dGoalsArray = getResources().getStringArray(R.array.diet_options);
        String[] tGoalsArray = getResources().getStringArray(R.array.goals_options);

        int genderIndex = -1;
        int dGoalsIndex = -1;
        int tGoalsIndex = -1;
        //get the index of the chosen option for all 3.
        for (int i = 0; i < genderArray.length; i++) {
            if (genderArray[i].equals(gender)) {
                genderIndex = i;
                break; // Exit the loop once a match is found
            }
        }

        for (int i = 0; i < dGoalsArray.length; i++) {
            if (dGoalsArray[i].equals(dGoals)) {
                dGoalsIndex = i;
                break; // Exit the loop once a match is found
            }
        }
        for (int i = 0; i < tGoalsArray.length; i++) {
            if (tGoalsArray[i].equals(tGoals)) {
                tGoalsIndex = i;
                break;
            }
        }
        //put them into a Map.
        //the index is used to make it easier with translation.
        Map<String, Object> preferences = new HashMap<>();
        preferences.put("gender", genderIndex);
        preferences.put("tGoals", tGoalsIndex);
        preferences.put("dGoals", dGoalsIndex);

        //Merge the preferenceMap with "preferences" in DB.
        userRef.set(preferences, SetOptions.merge()).addOnSuccessListener(unused -> {
            Toast.makeText(RegisterActivity.this, "Preferences Updated.",
                    Toast.LENGTH_SHORT).show();
            //Second step done.
            //Continue to mainAPP.
            continueMainApp();
        }).addOnFailureListener(e -> Toast.makeText(RegisterActivity.this, "Preferences Failed to update.",
                Toast.LENGTH_SHORT).show());
    }

    private void continueMainApp() {
        //Create the Intent.
        Intent in = new Intent(this, NavMain.class);
        Log.i("Default in Main", Locale.getDefault().toString());
        startActivity(in); //Start the activity.
    }

    public void signInWithGoogle() {
        //Create the intent from client.
        Intent i = client.getSignInIntent();
        startActivityForResult(i, 1234); //Start the activity with requestCode
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //If the requestCode is equal to 1234
        if (requestCode == 1234) {
            //Init the Task
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                //Try to get the account.
                GoogleSignInAccount account = task.getResult(ApiException.class);
                //Get the name and the email.
                String googleDisplayName = account.getDisplayName();
                String googleEmail = account.getEmail();
                //Get the credentials
                AuthCredential credential = GoogleAuthProvider.getCredential(account.getIdToken(), null);
                //Sign in with credential.
                FirebaseAuth.getInstance().signInWithCredential(credential)
                        .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if (task.isSuccessful()) {
                                    //If successful call handleSignInResult.
                                    handleSignInResult(googleDisplayName, googleEmail);
                                } else {
                                    Toast.makeText(RegisterActivity.this, "Error with sign in", Toast.LENGTH_LONG).show();
                                }
                            }
                        });
            } catch (ApiException e) {
                return;
            }
        }
    }

    private void handleSignInResult(String googleDisplayName, String googleEmail) {

        String userId = Objects.requireNonNull(FirebaseAuth.getInstance().getCurrentUser()).getUid();
        User user = new User(googleDisplayName, googleEmail); //Create new User.

        //Check if there is already a document with the field "username" equal to "googledisplayname".
        //If so the user already exist. send them to main app.
        db.collection("users").whereEqualTo("username", googleDisplayName)
                        .get().addOnCompleteListener( task -> {
                            if(task.isSuccessful()){
                                if(!task.getResult().isEmpty()){
                                    continueMainApp(); //Send them to main app.
                                }else{
                                    registerUserGoogle(userId, user); //Else create the user in DB.
                                }

                            }
                });

    }

    private void registerUserGoogle(String userId, User user) {
        //Save the user in database.
        db.collection("users").document(userId).set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Toast.makeText(RegisterActivity.this, "Register Success.",
                                Toast.LENGTH_SHORT).show();
                        //Next step in register.
                        nextFrame();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        //Make error message if failure.
                        Toast.makeText(RegisterActivity.this, "Internal Error try again later.",
                                Toast.LENGTH_SHORT).show();
                    }
                });
    }


}