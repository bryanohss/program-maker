package com.example.programmaker.auth;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.example.programmaker.R;
//Fragment for secondStep.
public class SecondaryRegister extends Fragment {
    public SecondaryRegister() {
        // Required empty public constructor
    }

    public static SecondaryRegister newInstance() {
        return new SecondaryRegister();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_secondary_register, container, false);
        //The Register Button
        Button registerBtn = v.findViewById(R.id.registerUser);
        //Call setUpSpinner to set up preference Spinners.
        setUpSpinners(v);
        //Set the ClickListener.
        registerBtn.setOnClickListener(view -> updatePreferences());
        return v;
    }

    private void updatePreferences() {
        //Get the parent activity
        RegisterActivity regActivity = (RegisterActivity) getActivity();
        assert regActivity != null;
        regActivity.updatePreferences(getGender(), getDGoals(), getTGoals()); //Call updatePreferences in Activity

    }

    private void setUpSpinners(View v) {
        //Get the Spiner
        Spinner genderSpn = v.findViewById(R.id.gender_spinner);
        //Set the adapter
        ArrayAdapter<CharSequence> gAdapter = ArrayAdapter.createFromResource(v.getContext(), R.array.gender_options, android.R.layout.simple_spinner_item);
        //Set the dropdown
        gAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        //set the adapter.
        genderSpn.setAdapter(gAdapter);

        //Same for the other spinners.
        Spinner goalSpn = v.findViewById(R.id.goals_spinner);
        ArrayAdapter<CharSequence> goalsAdapter = ArrayAdapter.createFromResource(v.getContext(), R.array.goals_options, android.R.layout.simple_spinner_item);
        goalsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        goalSpn.setAdapter(goalsAdapter);

        Spinner dietSpn = v.findViewById(R.id.diet_spinner);
        ArrayAdapter<CharSequence> dietAdapter = ArrayAdapter.createFromResource(v.getContext(), R.array.diet_options, android.R.layout.simple_spinner_item);
        dietAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dietSpn.setAdapter(dietAdapter);
    }

    @Override
    public void onPause() {
        //If the users pause the app
        //update preferences before that
        updatePreferences();
        super.onPause();
    }
    //Get
    private String getGender() {
        Spinner genderSpn = requireView().findViewById(R.id.gender_spinner);
        return genderSpn.getSelectedItem().toString();
    }
    private String getTGoals() {
        Spinner gSpn = requireView().findViewById(R.id.goals_spinner);
        return gSpn.getSelectedItem().toString();
    }

    private String getDGoals() {
        Spinner dSpn = requireView().findViewById(R.id.diet_spinner);
        return dSpn.getSelectedItem().toString();
    }

}