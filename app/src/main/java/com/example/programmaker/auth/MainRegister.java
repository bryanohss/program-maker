package com.example.programmaker.auth;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import androidx.fragment.app.Fragment;

import com.example.programmaker.R;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;

import java.util.Objects;
//First step in register.
public class MainRegister extends Fragment {

    public MainRegister() {
        // Required empty public constructor
    }
    public static MainRegister newInstance() {
        return new MainRegister();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View v = inflater.inflate(R.layout.fragment_main_register, container, false);
        //Configure the key components.
        Button nextBtn = v.findViewById(R.id.nextBtn);//Register Button
        nextBtn.setOnClickListener(view -> registerUser());  //Set the Listener
        ImageButton googleBtn = v.findViewById(R.id.google); //Register with Google Button
        googleBtn.setOnClickListener(view -> signInWithGoogle());  //Set the Listener
        return v;
    }

    private void signInWithGoogle() {
        ((RegisterActivity)requireActivity()).signInWithGoogle(); //Call method in the activity.
    }

    private void registerUser() {
        //Get the EditText from the form
        EditText username = requireView().findViewById(R.id.username);
        EditText email = requireView().findViewById(R.id.userEmail);
        EditText pass = requireView().findViewById(R.id.userPassword);
        //Call the method in the activity.
        ((RegisterActivity) requireActivity()).registerUser(username.getText().toString(), email.getText().toString(), pass.getText().toString());
    }



}
