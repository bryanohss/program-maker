package com.example.programmaker.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.programmaker.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
//Log Adapter
//Used to Display the Log of an Workout Day
public class LogAdapter extends RecyclerView.Adapter<LogAdapter.ViewHolder> {

    private final List<Object> logList;
    private final Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder{

        TextView workoutName;
        TextView date;
        LinearLayout parent;
        //Save the items in the source file in variables to user and modify later.
        public ViewHolder(View itemView) {
            super(itemView);
            workoutName = itemView.findViewById(R.id.workout_name_log);
            date = itemView.findViewById(R.id.log_date);
            parent = itemView.findViewById(R.id.parentLayout);

        }
    }

    public LogAdapter(Context context, List<Object> list){
        this.context = context;
        this.logList = list;

    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.log_item, parent, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LogAdapter.ViewHolder holder, int position) {
        //Get the data from the list.
        HashMap<String, Object> logData = (HashMap<String, Object>) logList.get(position);
        //Custom Font.
        Typeface typeface = ResourcesCompat.getFont(context, R.font.proxima);

        //Configure the UI of the workoutName and date text.
        holder.workoutName.setText(Objects.requireNonNull(logData.get("workout")).toString());
        holder.workoutName.setTextColor(ContextCompat.getColor(context, R.color.white));
        holder.workoutName.setTypeface(typeface);

        holder.date.setText((CharSequence) logData.get("date"));
        holder.date.setTextSize(20);
        holder.date.setTextColor(ContextCompat.getColor(context, R.color.white));
        holder.date.setTypeface(typeface);

        setUpExercieLog(holder, logData); //Send holder and logData to func.
    }

    private void setUpExercieLog(ViewHolder holder, HashMap<String, Object> logData) {
        //Get the list of exercises
        HashMap<String, Object> exerciseList = (HashMap<String, Object>) logData.get("exercies");
        assert exerciseList != null;
        Typeface typeface = ResourcesCompat.getFont(context, R.font.proxima);
        //For every exercise.
        for (Map.Entry<String, Object> entry : exerciseList.entrySet()) {
            //the name is the ket
            //configure the Text
            String exerciseName = entry.getKey();
            TextView exName = new TextView(context);
            exName.setText(exerciseName);
            exName.setTextColor(ContextCompat.getColor(context, R.color.white));
            exName.setTypeface(typeface);
            exName.setTextSize(25);
            //Create the firstParent using this func.
            LinearLayout fistParent = createFirstParen();

            fistParent.addView(exName); //Add the name to this parent.
            //setsInfo is equal to Ex. {info=[{sets=3, weight=0, reps=12}]}
            HashMap<String, Object> lodInfo = (HashMap<String, Object>) entry.getValue();
            ArrayList<Object> exerciseInfo = (ArrayList<Object>) lodInfo.get("info");

            //For every hash in exerciseInfo.
            for(int i = 0; i < Objects.requireNonNull(exerciseInfo).size(); i++){
                //Get current item.
                HashMap<String, Object> info = (HashMap<String, Object>) exerciseInfo.get(i);
                //Secondparent contains the titles "sets, reps and weight"
                LinearLayout secondParent = createSecondaryLayout();
                //thirdParent contains the amount of sets, reps and how much weight we did.
                LinearLayout thirdParent = createThirdParent(info);
                //Add these to the firstParent.
                fistParent.addView(secondParent);
                fistParent.addView(thirdParent);
            }
            //Add the first Parent to rootParent.
            holder.parent.addView(fistParent);


        }


    }

    private LinearLayout createFirstParen() {
        //Creates a linearLayout
        LinearLayout parent = new LinearLayout(context);
        parent.setOrientation(LinearLayout.VERTICAL);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        );

        parent.setLayoutParams(layoutParams);

        return parent;
    }

    private LinearLayout createSecondaryLayout() {
        //Font
        Typeface typeface = ResourcesCompat.getFont(context, R.font.proxima);
        //Create the parent.
        LinearLayout secondParent = new LinearLayout(context);
        secondParent.setOrientation(LinearLayout.HORIZONTAL);
        secondParent.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        ));
        //Create new TextViews.
        TextView setText = new TextView(context);
        TextView repsText = new TextView(context);
        TextView weightText = new TextView(context);
        //Configure them
        setText.setText(R.string.sets_text);
        setText.setTextColor(ContextCompat.getColor(context, R.color.white));
        setText.setTypeface(typeface);

        repsText.setText(R.string.reps_text);
        repsText.setTextColor(ContextCompat.getColor(context, R.color.white));
        repsText.setTypeface(typeface);

        weightText.setText(R.string.weight_text);
        weightText.setTextColor(ContextCompat.getColor(context, R.color.white));
        weightText.setTypeface(typeface);
        //Set their layoutParams
        setText.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1
        ));
        repsText.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1
        ));
        weightText.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1
        ));
        //Text Size
        setText.setTextSize(18);
        repsText.setTextSize(18);
        weightText.setTextSize(18);
        //add them to the parent
        secondParent.addView(setText);
        secondParent.addView(repsText);
        secondParent.addView(weightText);
        //return.
        return secondParent;
    }

    private LinearLayout createThirdParent(HashMap<String, Object> info) {
        //Font
        Typeface typeface = ResourcesCompat.getFont(context, R.font.proxima);
        //Create the parent.
        LinearLayout thirdParent = new LinearLayout(context);
        thirdParent.setOrientation(LinearLayout.HORIZONTAL);
        thirdParent.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT
        ));
        //Create the textViews
        TextView setText = new TextView(context);
        TextView repsText = new TextView(context);
        TextView weightText = new TextView(context);
        //Configure the textViews
        setText.setText(Objects.requireNonNull(info.get("sets")).toString());
        setText.setTextColor(ContextCompat.getColor(context, R.color.white));
        setText.setTypeface(typeface);

        repsText.setText(Objects.requireNonNull(info.get("reps")).toString());
        repsText.setTextColor(ContextCompat.getColor(context, R.color.white));
        repsText.setTypeface(typeface);

        weightText.setText(Objects.requireNonNull(info.get("weight")).toString());
        weightText.setTextColor(ContextCompat.getColor(context, R.color.white));
        weightText.setTypeface(typeface);

        //Set their LayoutParams
        setText.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1
        ));
        repsText.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1
        ));
        weightText.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1
        ));
        //Text size
        setText.setTextSize(18);
        repsText.setTextSize(18);
        weightText.setTextSize(18);
        //add them to the parent
        thirdParent.addView(setText);
        thirdParent.addView(repsText);
        thirdParent.addView(weightText);
        return thirdParent; //return them.

    }


    @Override
    public int getItemCount() {
        return logList.size();
    }
}
