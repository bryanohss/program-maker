package com.example.programmaker.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.programmaker.customC.Exercise;
import com.example.programmaker.R;

import java.util.HashMap;
import java.util.List;

//https://developer.android.com/develop/ui/views/layout/recyclerview#java
//ExerciseAdapter
//Displays the class Exercise in a professional way
public class ExerciseAdapter extends RecyclerView.Adapter<ExerciseAdapter.ViewHolder> {
    private final List<Exercise> exerciseList; //List of exercises
    private Context context; //Context
    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Save all the elements in the source file to variables.
        TextView exercieName;
        Button addRow;
        LinearLayout parent;

        public ViewHolder(View view) {
            super(view);
            exercieName = (TextView) view.findViewById(R.id.exercie_name);
            addRow = (Button) view.findViewById(R.id.add_row_btn);
            parent = (LinearLayout) view.findViewById(R.id.parent);
        }
    }
    //Constructor
    public ExerciseAdapter(Context context, List<Exercise> lst) {
        this.context = context;
        exerciseList = lst;
    }
    // onCreateViewHolder is called when RecyclerView needs a new ViewHolder
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.exercise_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override // onBindViewHolder is called to bind the data to a ViewHolder
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        Exercise exercise = exerciseList.get(position); //Get the exercise
        viewHolder.exercieName.setText(exercise.getName()); //Set the text in View
        List<HashMap<String, Object>> lst = exercise.getInfo(); //Get the info (Sets, Reps, Weight)
        //lst will be 1 in length when created this method is called.

        for (int i = 0; i < lst.size(); i++) { //i == ID for the tmp
            // Create and add rows dynamically
            LinearLayout tmp = createRowLayout(context , i, lst); // Call createRowLayout
            setUpFunctionality(tmp, i, lst); // Call setUpFunctionality
            viewHolder.parent.addView(tmp);
        }

        //Button to add Rows
        viewHolder.addRow.setOnClickListener(view -> {
            exercise.addColumnToInfo(); //Add More Rows in add the class
            List<HashMap<String, Object>> tmpLst = exercise.getInfo(); //get the Total List
            Integer id = tmpLst.size() -1; //The extra hashmap will be last in the lst therefore  -1
            LinearLayout tmp = createRowLayout(context , id, lst); //Create Row
            setUpFunctionality(tmp, id, lst); //set up the listeners.
            viewHolder.parent.addView(tmp); // add to parent.
        });
    }

    @SuppressLint("UseCompatTextViewDrawableApis") //i == ID
    //lst is the infoList [{sets:0, reps:0, weight:0}]
    public LinearLayout createRowLayout(Context context, Integer i, List<HashMap<String, Object>> lst){
        Integer finalId = i; //Final id is used for the Text Fields.
        HashMap<String, Object> tmpHash = new HashMap<>();
        tmpHash = lst.get(finalId); //tmpHash will contain the current item of [{sets:0, reps:0, weight:0},...]
        i = i*10; //Multiply i by 10 to get unique IDs for the Text Fields
        LinearLayout rowLayout = new LinearLayout(context); //Set up the RowLayout
        rowLayout.setLayoutParams(new ViewGroup.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ));
        rowLayout.setOrientation(LinearLayout.VERTICAL);
        rowLayout.setId(i); //Set the ID. First row, ID = 0

        //First Linear!
        //textLayout will contain the titels such ass "Sets, Reps, Weight"
        LinearLayout textLayout = new LinearLayout(context);
        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        //Set Margin
        layoutParams.setMargins(0,50,0,20);
        textLayout.setLayoutParams(layoutParams);
        textLayout.setOrientation(LinearLayout.HORIZONTAL);
        //Custom Font.
        Typeface typeface = ResourcesCompat.getFont(context, R.font.proxima);
        //Set Text
        TextView setText = new TextView(context);
        setText.setLayoutParams(new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
        ));
        //Configure the Text.
        setText.setGravity(Gravity.CENTER);
        setText.setText(R.string.set_text);
        setText.setTextSize(20);
        setText.setTextColor(ContextCompat.getColor(context, R.color.white));
        setText.setTypeface(typeface);

        //Reps Text
        TextView repsText = new TextView(context);
        repsText.setLayoutParams(new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
        ));
        repsText.setGravity(Gravity.CENTER);
        repsText.setText(R.string.reps_text);
        repsText.setTextSize(20);
        repsText.setTextColor(ContextCompat.getColor(context, R.color.white));
        repsText.setTypeface(typeface);

        //Weight Text
        TextView weiText = new TextView(context);
        weiText.setLayoutParams(new LinearLayout.LayoutParams(
                0,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
        ));
        weiText.setGravity(Gravity.CENTER);
        weiText.setText(R.string.weight_text);
        weiText.setTextSize(20);
        weiText.setTextColor(ContextCompat.getColor(context, R.color.white));
        weiText.setTypeface(typeface);
        //the these Texts to the textLayout.
        textLayout.addView(setText);
        textLayout.addView(repsText);
        textLayout.addView(weiText);
        //The the textLayout to the "parent layout".
        rowLayout.addView(textLayout);

        //Second Layout - TextEdits
        //editLayout wil contains the textFields
        LinearLayout editLayout = new LinearLayout(context);
        editLayout.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ));
        editLayout.setOrientation(LinearLayout.HORIZONTAL);
        //Set EditText
        EditText setEdit = new EditText(context);
        LinearLayout.LayoutParams layoutParams1 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1
        );
        //Configure the TextEdit.
        layoutParams1.setMargins(40,0,20,0);
        setEdit.setLayoutParams(layoutParams1);
        setEdit.setEms(10);
        setEdit.setGravity(Gravity.CENTER);
        setEdit.setHint(R.string.set_hint);
        setEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
        setEdit.setMinHeight(48);
        setEdit.setId(i+1); //FIRST LOOP i + 1 = 1, SECOND i + 1 == 11
        String setCount =  String.valueOf(tmpHash.get("sets"));
        setEdit.setText(setCount);
        //Visual Configuration.
        setEdit.setTextColor(ContextCompat.getColor(context, R.color.white));
        setEdit.setBackgroundResource(R.drawable.edit_text_background); //Custom Drawable File.
        setEdit.setEms(10); // Set the width to accommodate approximately 10 'M' characters.
        setEdit.setTypeface(typeface);

        //Rep EditText
        EditText repEdit = new EditText(context);
        repEdit.setLayoutParams(layoutParams1);
        repEdit.setEms(10);
        repEdit.setGravity(Gravity.CENTER);
        repEdit.setHint(R.string.set_hint);
        repEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
        repEdit.setMinHeight(48);
        repEdit.setId(i + 2); //FIRST LOOP i + 2 = 2, SECOND i + 2 == 12
        String repsCount = String.valueOf(tmpHash.get("reps"));
        repEdit.setText(repsCount);

        repEdit.setTextColor(ContextCompat.getColor(context, R.color.white));
        repEdit.setBackgroundResource(R.drawable.edit_text_background);
        repEdit.setEms(10);
        repEdit.setTypeface(typeface);
        //weight EditText
        EditText weEdit = new EditText(context);

        weEdit.setLayoutParams(layoutParams1);
        weEdit.setEms(10);
        weEdit.setGravity(Gravity.CENTER);
        weEdit.setHint(R.string.set_hint);
        weEdit.setInputType(InputType.TYPE_CLASS_NUMBER);
        weEdit.setMinHeight(48);
        weEdit.setId(i + 3); //FIRST LOOP i + 1 = 3, SECOND i + 1 == 13
        String weightCount = String.valueOf(tmpHash.get("weight"));
        weEdit.setText(weightCount);

        weEdit.setTextColor(ContextCompat.getColor(context, R.color.white));
        weEdit.setBackgroundResource(R.drawable.edit_text_background);
        weEdit.setEms(10);
        weEdit.setTypeface(typeface);
        //Add the editText to the layout
        editLayout.addView(setEdit);
        editLayout.addView(repEdit);
        editLayout.addView(weEdit);
        //Finale add the editLayout to the parent.
        rowLayout.addView(editLayout);

        return rowLayout;
    }

    private void setUpFunctionality(LinearLayout tmp, Integer id, List<HashMap<String, Object>> lst) {
        //Same method here. id = id*10 to target correct EditText.
        Integer finalId = id;
        id = id*10;
        EditText setEdit = tmp.findViewById(id+1); //First Loop id+1 == 1
        EditText repsEdit = tmp.findViewById(id+2); //First Loop id+2 == 2
        EditText weightEdit = tmp.findViewById(id+3); //First Loop id+3 == 3

        //TextChangeListener for setEdit.
        setEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                HashMap<String, Object> tmp = new HashMap<>();
                tmp = lst.get(finalId);
                if (!charSequence.toString().equals("")) { //If the character isnt null of empty.
                    tmp.replace("sets", Integer.parseInt(charSequence.toString())); // Replace the current number with the new One.
                    Log.i("it worked", lst.toString());
                } else {
                    tmp.replace("sets", 0); //else replace it with 0
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        //Same Listener as above
        repsEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                HashMap<String, Object> tmp = new HashMap<>();
                tmp = lst.get(finalId);
                if (!charSequence.toString().equals("")) {

                    tmp.replace("reps", Integer.parseInt(charSequence.toString()));
                    Log.i("it worked", lst.toString());
                } else {
                    tmp.replace("reps", 0);

                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        //Same Listener as above
        weightEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                HashMap<String, Object> tmp = new HashMap<>();
                tmp = lst.get(finalId);
                if (!charSequence.toString().equals("")) {

                    tmp.replace("weight", Integer.parseInt(charSequence.toString()));
                    Log.i("it worked", lst.toString());
                } else {
                    tmp.replace("weight", 0);

                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    @Override
    public int getItemCount() {
        return exerciseList.size();
    }
}

