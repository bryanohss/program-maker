package com.example.programmaker.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.programmaker.R;
import com.example.programmaker.customC.WorkoutPlan;

import java.util.List;

public class PShowCaseAdapter extends RecyclerView.Adapter<PShowCaseAdapter.ViewHolder> {

    //Program Showcase.
    //Shows programs.
    private final List<WorkoutPlan> lst;
    private Context context;
    private final ProgramAdapterListener listener;

    public interface ProgramAdapterListener{
        void showProgram(WorkoutPlan chosenOne);
    }
    public static class ViewHolder extends RecyclerView.ViewHolder {
        private final TextView nameText;
        private final ImageButton button;
        public ViewHolder(View view) {
            super(view);
            nameText = (TextView) view.findViewById(R.id.program_text);
            button = (ImageButton) view.findViewById(R.id.imageButton);

        }
    }

    public PShowCaseAdapter(Context context, List<WorkoutPlan> lst, ProgramAdapterListener listener){
        this.context = context;
        this.lst = lst;
        this.listener = listener;
    }

    @NonNull
    @Override
    public PShowCaseAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pshowcase_item, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull PShowCaseAdapter.ViewHolder holder, int position) {
        //Get the current item.
        WorkoutPlan chosenOne = lst.get(position);
        Log.i("cname", chosenOne.getName());
        //Set the name and listener.
        holder.nameText.setText(chosenOne.getName());
        holder.button.setOnClickListener(view -> showProgram(chosenOne));
    }
    //Call showProgram on the listener.
    private void showProgram(WorkoutPlan chosenOne) {
        listener.showProgram(chosenOne);
    }

    @Override
    public int getItemCount() {
        return lst.size();
    }
}
