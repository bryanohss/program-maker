package com.example.programmaker.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;

import com.example.programmaker.R;
import com.example.programmaker.customC.Exercise;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
//Custom string Adapter.
//Used for checklist.
public class CustomStringAdapter extends ArrayAdapter<String> {

    private final List<Exercise> selectedExercises; //List of selected exercises
    private final Map<String, Object> nameList; //List of exercise names

    public CustomStringAdapter(@NonNull Context context, int resource, List<String> items, List<Exercise> selectedExercises, Map<String, Object> nameList) {
        super(context, resource, items);
        this.selectedExercises = selectedExercises;
        this.nameList = nameList;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.custom_string_adapter_item, parent, false);
        }
        //Get the elements for the custom source file.
        CheckBox checkBox = convertView.findViewById(R.id.checkBox);
        TextView textView = convertView.findViewById(android.R.id.text1);
        textView.setTextColor(ContextCompat.getColor(getContext(), android.R.color.white));
        //Get the current item in "position"
        String item = getItem(position);
        if (item != null) {
            //Set the text with item.
            textView.setText(item);

            // Check if the item is in the selectedExercises list
            // If so, show the Checkbox as marked.
            boolean isSelected = false;
            for (Exercise i : selectedExercises) {
                String name = i.getName();
                //Modify the strings so its easier to get a match
                //Lower case and replace whitespace with "".
                String modifiedName = name.toLowerCase().replaceAll("\\s", "");
                String modifiedSecondName = item.toLowerCase().replaceAll("\\s", "");
                if (modifiedName.equals(modifiedSecondName)) {
                    isSelected = true;
                    break; // Exit the loop once a match is found
                }
            }

            // Set the CheckBox state based on whether the item is in selectedExercises
            checkBox.setChecked(isSelected);
        }
        //Listener for checkBox
        checkBox.setOnClickListener(v -> checkBoxListener(textView, checkBox) );

        return convertView;
    }

    private void checkBoxListener(TextView textView, CheckBox checkBox) {
        //Selected is the name of the exercise
        String selected = textView.getText().toString();
        //eSelected is the "Exercise" version of selected
        Exercise eSelected = (Exercise) nameList.get(selected);

        //selectedString will contain all the names in selectedExercises.
        List<String> selectedString = new ArrayList<>();
        for (Exercise tmp : selectedExercises) {
            selectedString.add(tmp.getName());
        }
        //If the checkBox is selected after click and selectedString doesnt contain selected, add selected exercise to
        //selectedExercises.
        if (checkBox.isChecked() && !selectedString.contains(selected)) {
            selectedExercises.add(eSelected);
        } else if (!checkBox.isChecked() && selectedString.contains(selected)) {
            selectedExercises.remove(eSelected); //Remove if the checkbox is not selected and selectedString contains "selected"
        }
    }

}


