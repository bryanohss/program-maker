package com.example.programmaker.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.example.programmaker.customC.Exercise;
import com.example.programmaker.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
//Shows the Workout.

public class WorkoutAdapter  extends RecyclerView.Adapter<WorkoutAdapter.ViewHolder> {
    //Listener
    private final WorkoutAdapterListener listener;
    private final Context context;
    private final HashMap<String, List<Exercise>> workouts; //List of workouts
    private final boolean edit; //boolean to decide which buttons to display.


    public interface WorkoutAdapterListener {
        void onWorkoutStartClicked(String workoutName);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        //Save the elements from the source file.
        TextView title;
        LinearLayout frame;

        public ViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.workout_name);
            frame = (LinearLayout) view.findViewById(R.id.vertical);
        }
    }

    public WorkoutAdapter(Context context, HashMap<String, List<Exercise>> map, WorkoutAdapterListener listener, boolean edit) {
        this.context = context;
        this.workouts = map;
        this.listener = listener;
        this.edit = edit;
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.workout_item, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        //List with all the names of the workouts.
        List<String> workoutNames = new ArrayList<>(workouts.keySet());
        //get the current workout now.
        String workoutName = workoutNames.get(position);
        viewHolder.title.setText(workoutName);
        //Call setUpView.
        setUpView(viewHolder, workoutName);
        //the edit booleans decides if we are able to edit the workout or just start them.
        if(!edit){
            setUpStartButton(viewHolder, workoutName);
        } else{
            setUpEditButton(viewHolder, workoutName);
        }

    }

    private void setUpEditButton(ViewHolder viewHolder, String workoutName) {
        //This function will allow the edit feature.
        Button edit = new Button(context);
        edit.setText(R.string.edit_text);
        //Set listener
        edit.setOnClickListener(view -> editWorkout(workoutName));
        //configure the buttons appearance
        edit.setTypeface(ResourcesCompat.getFont(context, R.font.proxima));
        edit.setTextColor(Color.parseColor("#242423"));
        edit.setBackgroundResource(R.drawable.button_background);
        edit.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        edit.setTypeface(null, Typeface.BOLD);

        ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        layoutParams.topMargin = 12;
        layoutParams.bottomMargin = 12;

        edit.setLayoutParams(layoutParams);

        viewHolder.frame.addView(edit);
    }

    private void editWorkout(String workoutName) {
        listener.onWorkoutStartClicked(workoutName);
    }

    private void setUpStartButton(ViewHolder viewHolder, String workoutName) {
        //Configure the start workout.
        Button start = new Button(context);
        start.setText(R.string.start);

        start.setOnClickListener(view -> startWorkout(workoutName));
        start.setTypeface(ResourcesCompat.getFont(context, R.font.proxima));
        start.setTextColor(Color.parseColor("#242423"));
        start.setBackgroundResource(R.drawable.button_background);
        start.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14);
        start.setTypeface(null, Typeface.BOLD);

        ViewGroup.MarginLayoutParams layoutParams = new ViewGroup.MarginLayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        layoutParams.topMargin = 12;
        layoutParams.bottomMargin = 12;
        start.setLayoutParams(layoutParams);

        viewHolder.frame.addView(start);
    }

    private void startWorkout(String workoutName) {
        listener.onWorkoutStartClicked(workoutName);
    }

    private void setUpView(ViewHolder viewHolder, String workoutName) {
        //Font
        Typeface typeface = ResourcesCompat.getFont(context, R.font.proxima);
        //get the list of exercises from the workout.
        List<Exercise> exercises = workouts.get(workoutName);
        //For every exercise.
        assert exercises != null;
        for( Exercise exercise : exercises){
            //Parent linear.
            LinearLayout tmpLinear = new LinearLayout(context);
            tmpLinear.setOrientation(LinearLayout.HORIZONTAL);
            tmpLinear.setLayoutParams(new ViewGroup.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            ));
            //Temp TextView. name of exercise.
            TextView tmp = new TextView(context);
            tmp.setLayoutParams(new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    1
            ));

            tmp.setTextSize(20);
            tmp.setText(exercise.getName());
            tmp.setTextColor(ContextCompat.getColor(context, R.color.white));
            tmp.setTypeface(typeface);
            //sets Text.
            TextView setsTmp = new TextView(context);
            setsTmp.setLayoutParams(new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    1
            ));
            tmp.setTextSize(20);
            int totalSets = 0;
            //Calculate the totals number of sew
            List<HashMap<String, Object>> tmpLst = exercise.getInfo();
            for (int i = 0; i < exercise.getInfo().size(); i++){
                Log.i("log", Objects.requireNonNull(tmpLst.get(i).get("sets")).toString());
                totalSets += (Long) tmpLst.get(i).get("sets");

            }
            //Configure UI
            setsTmp.setText(String.valueOf(totalSets));
            setsTmp.setGravity(Gravity.END);
            setsTmp.setTextSize(20);
            setsTmp.setTextColor(ContextCompat.getColor(context, R.color.white));
            setsTmp.setTypeface(typeface);

            tmpLinear.addView(tmp);
            tmpLinear.addView(setsTmp);
            viewHolder.frame.addView(tmpLinear);
    }


}
    @Override
    public int getItemCount() {
        return workouts.size();
    }

}
