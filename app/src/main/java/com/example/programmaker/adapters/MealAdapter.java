package com.example.programmaker.adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.bumptech.glide.Glide;
import com.example.programmaker.R;

import java.util.HashMap;
import java.util.List;
//Adapter used to show meals.
public class MealAdapter extends RecyclerView.Adapter<MealAdapter.ViewHolder> {
    //List of meals
    private final List<HashMap<String, Object>> mealList;
    //Listener to call methods.
    private final MealAdapterListener listener;
    //The interface
    public interface MealAdapterListener{
        void onMealClicked(HashMap<String, Object> clicked);
    }

    public MealAdapter(List<HashMap<String, Object>> mealList, MealAdapterListener listener){
        this.mealList = mealList;
        this.listener = listener;
    }
    public static class ViewHolder extends RecyclerView.ViewHolder{
        //Save the elements from the source file in variables
        ImageButton imageButton;
        TextView mealTitle;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            imageButton = (ImageButton) itemView.findViewById(R.id.imageButton);
            mealTitle = (TextView) itemView.findViewById(R.id.meal_text);

        }
    }


    @NonNull
    @Override
    public MealAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View v = LayoutInflater.from(parent.getContext())
               .inflate(R.layout.meal_item, parent, false);
       return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull MealAdapter.ViewHolder holder, int position) {
        HashMap<String, Object> chosenOne = mealList.get(position);
        Log.i("title", chosenOne.get("title").toString());
        Log.i("imageURL", chosenOne.get("ImageUrl").toString());
        //Set the title
        holder.mealTitle.setText(chosenOne.get("title").toString());
        //With Glide, set the image.
        String imageUrl = chosenOne.get("ImageUrl").toString();
        Glide.with(holder.itemView.getContext())
                .load(imageUrl)
                .into(holder.imageButton);
        //Set on clickListener.
        holder.imageButton.setOnClickListener(view -> mealClick(chosenOne));
    }
    //call Listener method.
    private void mealClick(HashMap<String, Object> chosenOne) {
        listener.onMealClicked(chosenOne);
    }

    @Override
    public int getItemCount() {
        return mealList.size();
    }
}
